Chef::Log.info("******Uninstall EDM Services******")

require 'win32/service'

# Override default EDM temp location to allow EDM services to be installed. This will be deleted later to resume normal EDM operation
# https://supportlibrary.markit.com/display/EDM/Overriding+Settings+and+Temporary+Files+Directories
powershell_script 'Set EDM Temporary Directory' do
  code '$filePath = "C:\\Program Files\\Markit Group\\Markit EDM\\Config\\CustomFileLocations.config"
	$XmlWriter = New-Object System.XMl.XmlTextWriter($filePath,$Null)
	$xmlWriter.Formatting = "Indented"
	$xmlWriter.Indentation = "4"
	$xmlWriter.WriteStartDocument()
	$xmlWriter.WriteStartElement("CustomFileLocations")
	$xmlWriter.WriteElementString("TempRootLocation","C:\\Program Files\\Markit Group")
	$xmlWriter.WriteEndElement()
	$xmlWriter.WriteEndDocument()
	$xmlWriter.Flush()
	$xmlWriter.Close()'
  not_if "Test-Path 'C:\\Program Files\\Markit Group\\Markit EDM\\Config\\CustomFileLocations.config'"
end

# Uninstall EDM Services
batch 'Uninstall EDMPL Service' do
  cwd 'C:\\Program Files\\Markit Group\\Markit EDM'
  code '"C:\\Program Files\\Markit Group\\Markit EDM\\CADIS.Service.Host.exe" /action:Uninstall /instancenumber:All'
  action :run
end

# Delete the old DataPorterPlugIns directory
directory 'C:\\Program Files\\Markit Group\\Markit EDM\\DataPorterPlugIns' do
  action :delete
end