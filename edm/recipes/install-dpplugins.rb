Chef::Log.info("******Deploy Data Porter PlugIns******")

# Use AWS SDK for S3 Object downloads
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Create required directories
directory 'D:\Software\EDM\DataPorterPlugIns\Unzipped' do
  recursive true
  action :create
end

# Download EDM Setup CD
powershell_script 'Download EDM Data Porter Plugins' do
  code <<-EOH
	# Declare variables
	$edmVersion = '#{node['software']['edm']['version']}'
	$edmVersionExists = Test-Path variable:\edmVersion
	$bucket = "edm-installers"
	$region = "us-east-1"
	$s3Key = "latest/DataPorterPlugIns.zip"
	$customS3Key = $edmVersion + "/DataPorterPlugIns.zip"
	$destination = "D:\\Software\\EDM\\DataPorterPlugIns.zip"

	# Download correct EDM version depending on value for Custom JSON in OpsWorks
	
	# If EDM version variable exists but is null then download the latest version
	If (!$edmVersion) {
		Read-S3Object -BucketName $bucket -Region $region -Key $s3Key -File $destination
	}

	# If EDM version variable exists then download the version specified
	ElseIf ($edmVersionExist -eq $_.true) {
		Read-S3Object -BucketName $bucket -Region $region -Key $customS3Key -File $destination
	}

	# In any other case, download the latest version of EDM
	Else {
		Read-S3Object -BucketName $bucket -Region $region -Key $s3Key -File $destination
	}
	EOH
  not_if {::File.exists?('D:\Software\EDM\DataPorterPlugIns.zip')}
end

# Unzip the Data Porter Plugins
powershell_script 'Unzip EDM Packages' do
  code <<-EOH
       $source = "d:\\software\\EDM\\DataPorterPlugIns.zip"
       $unzipDir = "d:\\Software\\EDM\\DataPorterPlugIns"
       $dirInfo = Get-ChildItem $unzipDir | Measure-Object
       Add-Type -assembly "system.io.compression.filesystem"
       If ($dirInfo.Count -eq 1) {
         [io.compression.zipfile]::ExtractToDirectory($source, $unzipDir)
         }
  EOH
end  

# Unzip the Data Porter Plugins to individual directories, then copy to PlugIn folder
powershell_script 'Deploy DPPlugIns' do
  code <<-EOH
  # Location of DPPlugIns zip files
  $source = "D:\\Software\\EDM\\DataPorterPlugIns"
  # Directory to unzip files to
  $unzipDirRoot = "D:\\Software\\EDM\\DataPorterPlugIns\\Unzipped"
  # Get directory size
  $dirInfo = Get-ChildItem $unzipDirRoot | Measure-Object
  # Get all zip files in the directory
  $list = Get-childitem $source -Recurse -Include *.zip
  # Add the zip assembly
  Add-Type -assembly "system.io.compression.filesystem"
  
  # If unzipDirRoot directory is empty then unzip all files to individual directories
  If ($dirInfo.Count -eq 0) {	
	foreach ($file in $list)
	{
		$unzipDir=$unzipDirRoot +"\\"+$file.BaseName
		echo "unzipping $file to $unzipDir"
		[io.compression.zipfile]::ExtractToDirectory($file, $unzipDir)
	}
  }

  If ($env:COMPUTERNAME -Like "*app*") {
	# PlugIn target directory
	$pluginDir = "C:\\Program Files\\Markit Group\\Markit EDM\\DataPorterPlugIns"
  }
  
  ElseIf ($env:COMPUTERNAME -Like "*web*") {
	# PlugIn target directory
	$pluginDir = "D:\\inetpub\\EDM\\bin\\DataPorterPlugIns"
  }
  
  Else {
	Write-Host "Unable to determine DPPlugIn destination based on computer name"
  }
  
  # Get the directory size
  $pluginDirInfo = Get-ChildItem $pluginDir | Measure-Object
  # Get all PlugIn files after being unzipped
  $pluginList = ChildItem $unzipDirRoot -Directory

  # If pluginDir is less than 10  then copy all PlugIns to the EDM PlugIns folder
  If ($pluginDirInfo.Count -lt 10) {
	foreach ($folder in $pluginList)
	{
		$pluginFiles = Get-ChildItem $folder.FullName
		Copy-Item $pluginFiles.FullName -Destination $pluginDir
		# Pause after each to prevent file open conflicts
		Start-Sleep -Seconds 10
	}
  }
  
  # Unblock files in case they get blocked
  Get-ChildItem "C:\\Program Files\\Markit Group\\Markit EDM" -Recurse | Unblock-File
  EOH
end  