Chef::Log.info("******Uninstall EDM Thick Client******")

# Uninstall EDM silently
windows_package 'Markit EDM Thick Client' do
  source 'C:\Program Files\Markit Group\Markit EDM\uninst.exe'
  options '/S'
  installer_type :custom
  action :install
  retries 3
  retry_delay 120
  only_if {::File.exists?('C:\Program Files\Markit Group\Markit EDM\CADIS.exe')}
end

# Delete the old DataPorterPlugIns directory
directory 'C:\\Program Files\\Markit Group\\Markit EDM\\DataPorterPlugIns' do
  recursive true
  action :delete
  only_if {::File.exists?('C:\Program Files\Markit Group\Markit EDM\DataPorterPlugIns')}
end

# Delete old Setup CDs and DPPlugins
powershell_script 'Clean Up Old Installers' do
  code <<-EOH
	# Check for Setup CDs and DPPlugIns for older versions
	$oldObjects = Get-ChildItem -Path "D:\\software\\EDM" | Where-Object { $_.Name -like "*Setup*" -or "*plugin*" }
	
	# if found, delete them
	foreach ($object in $oldObjects) {
		Remove-Item $object.FullName -Recurse
	}
	EOH
end