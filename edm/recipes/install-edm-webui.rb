Chef::Log.info("******Install EDM Web UI******")

# Install IIS
powershell_script 'Install IIS' do
  code 'Add-WindowsFeature Web-Server -IncludeAllSubfeature -IncludeManagementTools'
end

chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Create EDM Setup directory
directory 'D:\Software\EDM' do
  recursive true
  action :create
end

# Create EDM Web UI directory
directory 'D:\inetpub\EDM' do
  recursive true
  action :create
end

# Create EDM Web Service directory
directory 'D:\inetpub\Markit EDM Web Service' do
  recursive true
  action :create
end

# Download EDM Setup CD
powershell_script 'Download EDM Setup CD' do
  code <<-EOH
	# Declare varibles
	$edmVersion = '#{node['software']['edm']['version']}'
	$edmVersionExists = Test-Path variable:\edmVersion
	$bucket = "edm-installers"
	$region = "us-east-1"
	$s3Key = "latest/MarkitEDMSetupCD.zip"
	$customS3Key = $edmVersion + "/MarkitEDMSetupCD.zip"
	$destination = "D:\\Software\\EDM\\MarkitEDMSetupCD.zip"

	# Download correct EDM version depending on value for Custom JSON in OpsWorks
	
	# If EDM version variable exists but is null then download the latest version
	If (!$edmVersion) {
		Read-S3Object -BucketName $bucket -Region $region -Key $s3Key -File $destination
	}

	# If EDM version variable exists then download the version specified
	ElseIf ($edmVersionExist -eq $_.true) {
		Read-S3Object -BucketName $bucket -Region $region -Key $customS3Key -File $destination
	}

	# In any other case, download the latest version of EDM
	Else {
		Read-S3Object -BucketName $bucket -Region $region -Key $s3Key -File $destination
	}
	EOH
  not_if {::File.exists?('D:\Software\EDM\MarkitEDMSetupCD.zip')}
end

# Unzip the EDM setup CD
windows_zipfile 'D:\Software\EDM' do
  source 'd:\software\EDM\MarkitEDMSetupCD.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\EDM\Setup CD\Markit EDM Typical\MarkitEDMSetup.exe')}
end

# Rename the extracted setup CD directory to remove version number to keep standard
powershell_script 'Rename EDM Directory' do
  code 'Start-Sleep -Seconds 60
	$setupCD = Get-ChildItem -Path D:\\software -Recurse | Where-Object Name -Like "Setup CD*"
	Move-Item $SetupCD.FullName "D:\\Software\\EDM\\Setup CD"'
  not_if "Test-Path 'D:\\Software\\EDM\\Setup CD'"
end

# Deploy EDM Website and Web Service in IIS
powershell_script 'Markit EDM Web UI' do
  code	'Remove-Website -Name "Default Web Site"
	 xcopy "D:\Software\EDM\Setup CD\Web UI\*" "D:\inetpub\EDM\" /E /C /Y
	 xcopy "D:\Software\EDM\Setup CD\Markit EDM Web Service\*" "D:\inetpub\Markit EDM Web Service\" /E /C /Y
	 New-WebAppPool "EDM"
	 Set-ItemProperty IIS:\AppPools\EDM -Name enable32BitAppOnWin64 "True"
	 New-Website -Name "EDM" -Port 443 -Ssl -PhysicalPath "D:\Inetpub\EDM" -ApplicationPool "EDM"
	 $cert = New-SelfSignedCertificate -DnsName $env:COMPUTERNAME -CertStoreLocation Cert:\LocalMachine\My
	 (Get-WebBinding -Protocol HTTPS -Port 443).AddSslCertificate($cert.GetCertHashString(), "MY")
	 New-WebApplication -Name "Webservice" -Site "EDM" -PhysicalPath "D:\inetpub\Markit EDM Web Service\" -ApplicationPool "EDM"'
	 not_if "Test-Path 'D:\\inetpub\\EDM\\web.config'"
end

# Create Route53 A record
powershell_script 'Create Route 53 Record' do
  code <<-EOH
	# Zone name to check in Route 53
    $hostnameMetadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/hostname" -UseBasicParsing
    $fqdn = $hostnameMetadata.Content
    $domain = $fqdn.Split('.')[+1]
    $zonename = $domain + ".com."
    $domainname = $domain + ".com"

    # Get instance Id from metadata
    $instanceMetadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/instance-id" -UseBasicParsing
    $instanceId = $instanceMetadata.Content

    # Get region by trimming the last character off the AZ from metadata
    $azMetadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/placement/availability-zone" -UseBasicParsing
    $az = $azMetadata.Content
    $region = $az.Substring(0,$az.Length-1)

    # Get hostname tag from EC2
    $instance = (Get-EC2Instance -Instance $instanceId -Region $region).Instances
    $tag = $instance.Tags | Where-Object { $_.Key -eq "opsworks:instance" }
    $hostname = $tag.Value
	
    # Locate the zone name in Route 53
    $zone = Get-R53HostedZonesByName | Where-Object { $_.Name -EQ $zonename }
    # Select the ID and remove the first 12 characters to get the Hosted Zone ID
    $zone.Id
    $zoneId = $zone.Id.Substring(12)

    # Get the EIP to create the A record
    $metadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/public-ipv4" -UseBasicParsing
    $eip = $metadata.Content

    # Search for a Hosted Zone with matching Zone ID
    $domain = Get-R53ResourceRecordSet -HostedZoneId $zoneId
    # List all resource records in the zone
    $records = $domain.ResourceRecordSets.Name
    # FQDN to check for in Route 53
    $matchRecord = $hostname + "." + $zonename

    # Create a new resource record for the server if it does not already exist
    if ($records -eq $matchRecord) {
    	write-host "record already exists"
    }

    else {
    	# A record to be created
    	$aRecord = $hostname + "." + $domainname
    	
    	# Build up parameters to add
    	$record = New-Object Amazon.Route53.Model.Change
    	$record.Action = "CREATE"
    	$record.ResourceRecordSet = New-Object Amazon.Route53.Model.ResourceRecordSet
    	$record.ResourceRecordSet.Name = $aRecord
    	$record.ResourceRecordSet.Type = "A"
    	$record.ResourceRecordSet.TTL = 600
    	$record.ResourceRecordSet.ResourceRecords.Add(@{Value=$eip})

    	$params = @{
    	HostedZoneId=$zoneId
    	ChangeBatch_Comment="Create A record to resolve EIP to FQDN"
    	ChangeBatch_Change=$record
    	}

    # Create the A record in Route 53
    Edit-R53ResourceRecordSet @params
    }
  EOH
end