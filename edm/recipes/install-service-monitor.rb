﻿Chef::Log.info("******Install EDM Service Monitor*****")

	# Configure EDM Service Monitor
powershell_script 'Install EDM Service Monitor' do
	code <<-EOH
		$region = "us-east-1"
		$bucket = "mkt-cfn-scripts"
		$scriptLocation = "C:\\Scripts\\EDMServiceMonitor.ps1"
		$key = "EDMServiceMonitor.ps1"
		$domain = $env:USERDOMAIN
		$username = Join-Path $domain "\Task_Sched"
		$password = '#{node['servicemonitor']['serviceMonPwd']}'
		Read-S3Object -BucketName $bucket -Region $region -Key $key -File $scriptLocation
		Start-Sleep -Seconds 10
		$action = New-ScheduledTaskAction -Execute 'Powershell.exe' -Argument '-file "c:\\Scripts\\EDMServiceMonitor.ps1"'
		$trigger =  New-ScheduledTaskTrigger -Once -At (Get-Date) -RepetitionInterval (New-TimeSpan -Minutes 60) -RepetitionDuration ([System.TimeSpan]::MaxValue)
		Register-ScheduledTask -Action $action -Trigger $trigger -user $username -password $password -TaskName "EDM Service Monitor" -Description "Runs C:\Scripts\EDMServiceMonitior.ps1" -RunLevel Highest  
	EOH
end

