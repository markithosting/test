Chef::Log.info("******Configure IIS Timeouts******")

powershell_script 'Configure IIS Timeouts' do
	code <<-EOH
	# First, we get the cmdlets needed for interaction with IIS
	Import-Module WebAdministration

	# Next, we set some parameters that we will be using later.
	$name = "EDM"
	$site = 'IIS:\\Sites\\' + $name
	$appPool = 'IIS:\\AppPools\\' + $name

	# We get the full path for the location of the Web.Config associated with this site
	# (Get-WebConfigFile $site).FullName IIS
	$webConfig = "D:\\inetpub\\EDM\\web.config"
	$xml = [xml](Get-Content $webConfig)

	# We then look for a specific node and change it's value
	$node = $xml.Configuration.appSettings.add | where {$_.Key -eq 'Timeout'}
	$node.value = '60'

	# We save the changes back to the file
	$xml.Save($webConfig)

	# We set the idletimeout time to 60 mins here
	Set-ItemProperty ($appPool) -Name processModel.idleTimeout -value ( [TimeSpan]::FromMinutes(60))

	# And echo back the results to show it has been successful
	Get-ItemProperty ($appPool) -Name processModel.idleTimeout.value

	# Sleep for 1 seconds to allow to finish
	Start-Sleep -Seconds 1

	# Restart our App Pool
	Restart-WebAppPool $name

	# Wait for 3 seconds whilst Pool restarts
	Start-Sleep -Seconds 3
	
	# Then restart the site.
	Restart-WebItem $site
	EOH
end