Chef::Log.info("******Download Packages******")

# Download packages
powershell_script 'Unzip EDM Packages' do
  code <<-EOH
  # Define bucket details
  $region = "us-east-1"
  $bucket = "edm-packages"

  # Get the characters 3 and 4 from the computer name, this is the prefix name in the bucket
  $prefix = $env:computername.substring(2,2)
  $bucketName = Join-Path $bucket $prefix

  # List all packages in the bucket
  $packages = Get-S3Object -BucketName $bucket -KeyPrefix $prefix -Region $region | Where-Object { $_.Key -like "*.*" }

  # Download each Package
  foreach ($package in $packages) {
    $localPackages = "d:\\Software\\EDM\\Packages"
    $keyPath = $package.Key
    $key = $keyPath.Substring(2)
    $localPath = Join-Path $localPackages $key
    Read-S3Object -BucketName $bucket -Region $region -Key $keyPath -File $localPath
  }
  EOH
end