Chef::Log.info("******Configure Web.config******")

# Use AWS SDK for S3 Object downloads
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Download Encryption Utility
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'EncryptionUtility.zip',
                     response_target: 'd:\software\EDM\EncryptionUtility.zip')
                    
  end
  action :run
  not_if {::File.exists?('d:\software\EDM\EncryptionUtility.zip')}
end

# Unzip the file
windows_zipfile 'D:\Software\EDM' do
  source 'd:\software\EDM\EncryptionUtility.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\EDM\EncryptionUtility\CADIS.Encryption.Utility.exe')}
end

powershell_script 'Configure Web.config' do
  code <<-EOH
  # Declare variables
  $edmWebConfig = 'D:\\inetpub\\edm\\Web.config'
  $svcWebConfig = 'D:\\inetpub\\Markit EDM Web Service\\Web.config'
  $server = '#{node['dbdetails']['endpoint']}'
  $database = '#{node['dbdetails']['edmDbName']}'
  $username = '#{node['dbdetails']['sqlLogin']}'
  $password = '#{node['dbdetails']['sqlPwd']}'
  $passwordPath = 'C:\\windows\\temp\\encrypt.txt'
  
  # Generate the encrypted password and save to the path above
  $generatePassword = D:\\software\\edm\\EncryptionUtility\\CADIS.Encryption.Utility.exe /password:$password | Out-File $passwordPath

  # Read the encrypted file from the previous command and store in a variable
  $encryptedPassword = (Get-Content $passwordPath)

  # Read web.config file
  $doc = [xml](Get-Content $edmWebConfig)

  # Find the Server Node and set it to be the variable above
  $serverNode = $doc.configuration.appSettings.add | Where-Object {$_.Key -eq 'Server'}
  $serverNode.Value = $server

  # Find the Database Node and set it to be the variable above
  $dbNode = $doc.configuration.appSettings.add | Where-Object {$_.Key -eq 'Database'}
  $dbNode.Value = $database

  # Find the Username Node and set it to be the variable above
  $userNode = $doc.configuration.appSettings.add | Where-Object {$_.Key -eq 'Username'}
  $userNode.Value = $username

  # Find the Password Node and set it to be the variable above
  $passwordNode = $doc.configuration.appSettings.add | Where-Object {$_.Key -eq 'Password'}
  $passwordNode.Value = [string]$encryptedPassword

  # Save the file
  $doc.Save($edmWebConfig) -as [xml]

  #############Repeat the process for the Web Service web.config#############
  # Read web.config file
  $doc = [xml](Get-Content $svcWebConfig)

  # Find the Server Node and set it to be the variable above
  $serverNode = $doc.configuration.appSettings.add | Where-Object {$_.Key -eq 'Server'}
  $serverNode.Value = $server

  # Find the Database Node and set it to be the variable above
  $dbNode = $doc.configuration.appSettings.add | Where-Object {$_.Key -eq 'Database'}
  $dbNode.Value = $database

  # Find the Username Node and set it to be the variable above
  $userNode = $doc.configuration.appSettings.add | Where-Object {$_.Key -eq 'Username'}
  $userNode.Value = $username

  # Find the Password Node and set it to be the variable above
  $passwordNode = $doc.configuration.appSettings.add | Where-Object {$_.Key -eq 'Password'}
  $passwordNode.Value = [string]$encryptedPassword

  # Save the file
  $doc.Save($svcWebConfig) -as [xml]
  
  # Delete the temp file containing the encrypted password
  Remove-Item $passwordPath
  EOH
end

# Set the Application Pool Identity
powershell_script 'Markit EDM Web UI' do
  code	<<-EOH
  Import-Module WebAdministration
  
  # Extract the domain name from AWS metadata 
  $hostname = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/hostname" -UseBasicParsing
  $fqdn = $hostname.Content
  $domain = $fqdn.Split('.')[+1]
  
  # Work out the username depending on the environment and store in a variable
  If ($env:COMPUTERNAME -like "*dv*") {
  $username = $domain+"\\edm_dev_service"
  }
  ElseIf ($env:COMPUTERNAME -like "*ut*") {
    $username = $domain+"\\edm_uat_service"
  }
  ElseIf ($env:COMPUTERNAME -like "*pd*") {
    $username = $domain+"\\edm_prod_service"
  }
  $password = '#{node['dbdetails']['sqlPwd']}'
  
  # Set the identity on the App Pool to run as the service account
  $appPool = Get-Item IIS:\\AppPools\\EDM
  $appPool.processModel.userName = $username
  $appPool.processModel.password = $password
  $appPool.processModel.identityType = 3
  $appPool | Set-Item
  EOH
end