Chef::Log.info("******Generate Console Settings******")

# Use AWS SDK for S3 Object downloads
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Create directory for Console Settings Generator App
directory 'D:\Software\EDM\ConsoleSettingsGenerator' do
  recursive true
  action :create
end

# Download Encryption Utility
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'ConsoleSettingsGenerator.zip',
                     response_target: 'd:\software\EDM\ConsoleSettingsGenerator.zip')
                    
  end
  action :run
  not_if {::File.exists?('d:\software\EDM\ConsoleSettingsGenerator.zip')}
end

# Unzip the file
windows_zipfile 'D:\Software\EDM\ConsoleSettingsGenerator' do
  source 'd:\software\EDM\ConsoleSettingsGenerator.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\EDM\ConsoleSettingsGenerator\ConsoleSettingsGenerator.exe')}
end

powershell_script 'Generate Console Settings' do
  code <<-EOH
	# Declare variables
	$server = '#{node['dbdetails']['endpoint']}'
	$company = '#{node['aws-tag']['tags']['Company']}'
	$tempPath = 'D:\\Software\\EDM\\'
	$consoleFilePath = 'C:\\Users\\Default\\AppData\\Roaming\\MarkitEDM Settings\\'
	$consoleFile = $tempPath + "ConsoleSettings110.xml"
	$consoleFileNew = $consoleFilePath + "ConsoleSettings105.xml"
	$consoleFileRelease = $tempPath + "ConsoleSettings105-Release.xml"
	$consoleApp = 'D:\\Software\\EDM\\ConsoleSettingsGenerator\\ConsoleSettingsGenerator.exe'
	
	# Set the DB name and Service account name based on the environment, determined by computer name
	If ($env:COMPUTERNAME -like "*dv*") {
    	$database = "EDM_DEV"
		$username = "EDM_DEV_SERVICE"
		$password = '#{node['dbdetails']['sqlPwd']}'
		$description = $company + "_" + $database
		$arguments = $server + " " + $database + " " + $description + " " + $username + " " + $password + " " + $tempPath

		# Create the required folder in the default user profile
		New-Item -ItemType Directory -Path $consoleFilePath -Force

		# Create the Console Settings File
		Start-Process -FilePath $consoleApp -ArgumentList $arguments

		# Wait for file to be created
		Start-Sleep -Seconds 5

		# Move the file to the default user profile and rename for EDM v10.5
		Move-Item $consoleFile $consoleFileNew -Force
	}
	ElseIf ($env:COMPUTERNAME -like "*ut*") {
		$database = "EDM_UAT"
		$username = "EDM_UAT_SERVICE"
		$password = '#{node['dbdetails']['sqlPwd']}'
		$description = $company + "_" + $database
		$arguments = $server + " " + $database + " " + $description + " " + $username + " " + $password + " " + $tempPath

		# Create the required folder in the default user profile
		New-Item -ItemType Directory -Path $consoleFilePath -Force

		# Create the Console Settings File
		Start-Process -FilePath $consoleApp -ArgumentList $arguments

		# Wait for file to be created
		Start-Sleep -Seconds 5

		# Move the file to the default user profile and rename for EDM v10.5
		Move-Item $consoleFile $consoleFileNew -Force
  	}
  	ElseIf ($env:COMPUTERNAME -like "*pd*") {
    	$database = "EDM_PROD"
		$edmSupportUser = "EDMSupport"
		$edmReleaseUser = "EDMRelease"
		$edmSupportPwd = '#{node['dbdetails']['EDMSupportPwd']}'
		$edmReleasePwd = '#{node['dbdetails']['EDMReleasePwd']}'
		$description = $company + "_" + $database
		
		# Arguments to create console settings for EDMSupport 
		$EDMSupportArguments = $server + " " + $database + " " + $description + " " + $edmSupportUser + " " + $edmSupportPwd + " " + $tempPath
			
		# Arguments to create console settings for EDMRelease 
		$EDMReleaseArguments = $server + " " + $database + " " + $description + " " + $edmReleaseUser + " " + $edmReleasePwd + " " + $tempPath

		# Create the required folder in the default user profile
		New-Item -ItemType Directory -Path $consoleFilePath -Force

		# Create the Console Settings File for EDMSupport
		Start-Process -FilePath $consoleApp -ArgumentList $EDMSupportArguments
			
		# Wait for files to be created
		Start-Sleep -Seconds 5

		# Move the file to the default user profile and rename for EDM v10.5
		Move-Item $consoleFile $consoleFileNew -Force
		
		# Create the Console Settings File for EDMRelease
		Start-Process -FilePath $consoleApp -ArgumentList $EDMReleaseArguments
		
		# Wait for files to be created
		Start-Sleep -Seconds 5

		# Rename the Console Settings file for EDMRelease
		Move-Item $consoleFile $consoleFileRelease -Force
  	}
	Else {
		write-host "Cannot determine DB name based on computer name"
	}
  EOH
end

# Delete Console Settings Generator files and directories
file 'd:\software\EDM\ConsoleSettingsGenerator.zip' do
  action :delete
end