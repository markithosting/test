Chef::Log.info("******Install EDM Services******")

require 'win32/service'

# Override default EDM temp location to allow EDM services to be installed. This will be deleted later to resume normal EDM operation
# https://supportlibrary.markit.com/display/EDM/Overriding+Settings+and+Temporary+Files+Directories
powershell_script 'Set EDM Temporary Directory' do
  code '$filePath = "C:\\Program Files\\Markit Group\\Markit EDM\\Config\\CustomFileLocations.config"
	$XmlWriter = New-Object System.XMl.XmlTextWriter($filePath,$Null)
	$xmlWriter.Formatting = "Indented"
	$xmlWriter.Indentation = "4"
	$xmlWriter.WriteStartDocument()
	$xmlWriter.WriteStartElement("CustomFileLocations")
	$xmlWriter.WriteElementString("TempRootLocation","C:\\Program Files\\Markit Group")
	$xmlWriter.WriteEndElement()
	$xmlWriter.WriteEndDocument()
	$xmlWriter.Flush()
	$xmlWriter.Close()'
  not_if "Test-Path 'C:\\Program Files\\Markit Group\\Markit EDM\\Config\\CustomFileLocations.config'"
end

# Install EDM Process Launcher service
batch 'Install EDMPL Service' do
  cwd 'C:\\Program Files\\Markit Group\\Markit EDM'
  code '"C:\\Program Files\\Markit Group\\Markit EDM\\CADIS.Service.Host.exe" /action:Install /instancenumber:1 /description:"Process Launcher"'
  action :run
  not_if {::Win32::Service.exists?('Markit EDM Service Host 001')}
end

# Install EDM Event Watcher service
batch 'Install EDMEV Service' do
  cwd 'C:\\Program Files\\Markit Group\\Markit EDM'
  code '"C:\\Program Files\\Markit Group\\Markit EDM\\CADIS.Service.Host.exe" /action:Install /instancenumber:2 /description:"Event Watcher"'
  action :run
  not_if {::Win32::Service.exists?('Markit EDM Service Host 002')}
end

# Set EDM Event Watcher service to Automatic
windows_service 'Markit EDM Service Host 001' do
  action :configure_startup
  startup_type :automatic
end

# Set EDM Process Launcher service to Automatic
windows_service 'Markit EDM Service Host 002' do
  action :configure_startup
  startup_type :automatic
end

# Grant log on as a service - taken from https://gallery.technet.microsoft.com/scriptcenter/Grant-Log-on-as-a-service-11a50893
#powershell_script 'Grant Logon as a Service' do
#  code <<-EOH
#  c:\chef\cookbooks\files\LogOnAsService.ps1'
#end

  # Set service logon details
  # Chef run_as_user property on windows_service does not run on Chef 12.2 so this code is a workaround
powershell_script 'Set Service Logon' do
  code <<-EOH
  
  # Extract the domain name from AWS metadata
  $hostname = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/hostname" -UseBasicParsing
  $fqdn = $hostname.Content
  $domain = $fqdn.Split('.')[+1]
  
  # Work out the username depending on the environment and store in a variable
  If ($env:COMPUTERNAME -like "*dv*") {
  $username = $domain+"\\edm_dev_service"
  }
  ElseIf ($env:COMPUTERNAME -like "*ut*") {
    $username = $domain+"\\edm_uat_service"
  }
  ElseIf ($env:COMPUTERNAME -like "*pd*") {
    $username = $domain+"\\edm_prod_service"
  }
  c:\\chef\\cookbooks\\files\\LogOnAsService.ps1 $username
  $password = '#{node['dbdetails']['sqlPwd']}'
  $EDMPL = gwmi win32_service -filter "name='Markit EDM Service Host 001'"
  $EDMPL | Invoke-WmiMethod -Name Change -ArgumentList @($null,$null,$null,$null,$null,$null,$null,$null,$null,$username,$password)
  $EDMEV = gwmi win32_service -filter "name='Markit EDM Service Host 002'"
  $EDMEV | Invoke-WmiMethod -Name Change -ArgumentList @($null,$null,$null,$null,$null,$null,$null,$null,$null,$username,$password)
  EOH
end

# Delete the config file which overrides EDM Temp location to resume normal EDM operation
file 'C:\\Program Files\\Markit Group\\Markit EDM\\Config\\CustomFileLocations.config' do
  action :delete
end