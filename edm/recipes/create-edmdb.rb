Chef::Log.info("******Configure Databases******")

# Use AWS SDK for S3 Object downloads
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Create SQL Scripts local directory
directory 'D:\Software\SQLScripts' do
  recursive true
  action :create
end

# Download SQL Scripts
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'SQLScripts.zip',
                     response_target: 'd:\software\SQLScripts.zip')
                    
  end
  action :run
  not_if {::File.exists?('D:\Software\SQLScripts.zip')}
end

# Download EDM License
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')
    
    s3_key = node['domain'] + '/' + 'license.sql'

    s3_client.get_object(bucket: 'edm-licenses',
                     key: s3_key,
                     response_target: 'd:\software\EDM\license.sql')
                    
  end
  action :run
  not_if {::File.exists?('D:\Software\EDM\License.sql')}
end

# Unzip SQL Scripts
windows_zipfile 'd:\software\SQLScripts' do
  source 'D:\Software\SQLScripts.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\SQLScripts\serviceusers.sql')}
end

powershell_script 'Configure Scripts' do
  code <<-EOH
	# Script locations
	$docsDboTblsScript = "D:\\Software\\SQLScripts\\documentsdbotables.sql"
	$indexRebuildScript = "D:\\Software\\SQLScripts\\indexrebuild.sql"
	$serviceUsersScript = "D:\\Software\\SQLScripts\\serviceusers.sql"
	$supportUsersScript = "D:\\Software\\SQLScripts\\supportusers.sql"
	$indexOptimiseScript = "D:\\Software\\SQLScripts\\indexoptimise.sql"
	$sqlPwd = '#{node['dbdetails']['sqlPwd']}'

	# Set the DB name and Service account name based on the environment, determined by computer name
	If ($env:COMPUTERNAME -like "*dv*") {
    		$edmDbName = "EDM_DEV"
    		$svcName = "EDM_DEV_SERVICE"
  	}
  	ElseIf ($env:COMPUTERNAME -like "*ut*") {
    		$edmDbName = "EDM_UAT"
    		$svcName = "EDM_UAT_SERVICE"
  	}
  	ElseIf ($env:COMPUTERNAME -like "*pd*") {
    		$edmDbName = "EDM_PROD"
    		$svcName = "EDM_PROD_SERVICE"
  	}

	# Read scripts for dbname and svcname and replace with name according to the environment
	(Get-Content $docsDboTblsScript) -replace 'dbname', $edmDbName | Set-Content $docsDboTblsScript
	(Get-Content $indexRebuildScript) -replace 'dbname', $edmDbName | Set-Content $indexRebuildScript
	(Get-Content $serviceUsersScript) -replace 'dbname', $edmDbName | Set-Content $serviceUsersScript
	(Get-Content $serviceUsersScript) -replace 'svcname', $svcName | Set-Content $serviceUsersScript
	(Get-Content $serviceUsersScript) -replace 'sqlPwd', $sqlPwd | Set-Content $serviceUsersScript
	(Get-Content $supportUsersScript) -replace 'dbname', $edmDbName | Set-Content $supportUsersScript
	(Get-Content $indexOptimiseScript) -replace 'dbname', $edmDbName | Set-Content $indexOptimiseScript
  EOH
end

powershell_script 'Create Database' do
  code <<-EOH
	#Function to import SQLPS module taken from http:///mikefrobbins.com//2015//06/25//function-to-import-the-sqlps-powershell-module-or-snap-in
	function Import-SqlModule {
    		[CmdletBinding()]
   		param ()
    		if (-not(Get-Module -Name SQLPS) -and (-not(Get-PSSnapin -Name SqlServerCmdletSnapin100, SqlServerProviderSnapin100 -ErrorAction SilentlyContinue))) {
    		Write-Verbose -Message 'SQLPS PowerShell module or snapin not currently loaded'
 
        		if (Get-Module -Name SQLPS -ListAvailable) {
        		Write-Verbose -Message 'SQLPS PowerShell module found'
 
            			Push-Location
            			Write-Verbose -Message "Storing the current location: '$((Get-Location).Path)'"
 
            			if ((Get-ExecutionPolicy) -ne 'Restricted') {
                		Import-Module -Name SQLPS -DisableNameChecking -Verbose:$false
                		Write-Verbose -Message 'SQLPS PowerShell module successfully imported'
            			}
            			else{
                		Write-Warning -Message 'The SQLPS PowerShell module cannot be loaded with an execution policy of restricted'
            			}
            
            			Pop-Location
            			Write-Verbose -Message "Changing current location to previously stored location: '$((Get-Location).Path)'"
        		}
        		elseif (Get-PSSnapin -Name SqlServerCmdletSnapin100, SqlServerProviderSnapin100 -Registered -ErrorAction SilentlyContinue) {
        		Write-Verbose -Message 'SQL PowerShell snapin found'
 
            			Add-PSSnapin -Name SqlServerCmdletSnapin100, SqlServerProviderSnapin100
            			Write-Verbose -Message 'SQL PowerShell snapin successfully added'
 
            			[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.Smo') | Out-Null
            			Write-Verbose -Message 'SQL Server Management Objects .NET assembly successfully loaded'
        		}
        		else {
            			Write-Warning -Message 'SQLPS PowerShell module or snapin not found'
        		}
    		}
    		else {
        		Write-Verbose -Message 'SQL PowerShell module or snapin already loaded'
    		}
 
	}

	#Import the previously created module
	Import-SqlModule       	

	[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.SMO') | out-null
    
	#Declare all variables
	$serverName = '#{node['dbdetails']['endpoint']}'
	$edmDbName = '#{node['dbdetails']['edmDbName']}'
	$vcDbName = '#{node['dbdetails']['vcDbName']}'
	$adminDbName = "DBAdmin"
	$adminUser = '#{node['dbdetails']['adminUser']}'
	$adminPwd = '#{node['dbdetails']['adminPwd']}'
	$supportUsersScript = "D:\\Software\\SQLScripts\\supportusers.sql"
	$serviceUsersScript = "D:\\Software\\SQLScripts\\serviceusers.sql"
	$docsDboTblScript = "D:\\Software\\SQLScripts\\documentsdbotables.sql"
	$indexRebuildScript = "D:\\Software\\SQLScripts\\indexrebuild.sql"
	$adminDbScript = "D:\\Software\\SQLScripts\\admindb.sql"
	$indexOptimiseScript = "D:\\Software\\SQLScripts\\indexoptimise.sql"
	$edmScripts = "D:\\Software\\EDM\\Setup CD\\New Database\\01_AllScripts.sql"
	$edmProcs = "D:\\Software\\EDM\\Setup CD\\New Database\\20_AllProcs.sql"
	$vcScripts = "D:\\Software\\EDM\\Setup CD\\Version Control\\10_VCTables.sql"
	$vcProcs = "D:\\Software\\EDM\\Setup CD\\Version Control\\20_VCProcs.sql"
	$edmLicense = "D:\\Software\\EDM\\license.sql"
	$server = new-object ('Microsoft.SqlServer.Management.Smo.Server') $serverName
	$server.ConnectionContext.LoginSecure=$false;
	$server.ConnectionContext.set_Login($adminUser)
	$securePassword = ConvertTo-SecureString $adminPwd -AsPlainText -Force
	$server.ConnectionContext.set_SecurePassword($securePassword)
	$server.ConnectionContext.ApplicationName="SQLDeploymentScript"

	#Create the EDM DB
	$edmDb = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Database -argumentlist $server, $edmDbName
	$edmDb.Create()

	#Create the Version Control DB
	$vcDb = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Database -argumentlist $server, $vcDbName
	$vcDb.Create()

	#Create the Admin DB
	$adminDb = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Database -argumentlist $server, $adminDbName
	$adminDb.Create()

	#Run EDM scripts from setup CD against the EDM database
	Invoke-Sqlcmd -InputFile $edmScripts -ServerInstance $serverName -Database $edmDbName -Username $adminUser -Password $adminPwd
	Invoke-Sqlcmd -InputFile $edmProcs -ServerInstance $serverName -Database $edmDbName -Username $adminUser -Password $adminPwd

	#Run Version Control scripts from setup CD against the Version Control database
	Invoke-Sqlcmd -InputFile $vcScripts -ServerInstance $serverName -Database $vcDbName -Username $adminUser -Password $adminPwd
	Invoke-Sqlcmd -InputFile $vcProcs -ServerInstance $serverName -Database $vcDbName -Username $adminUser -Password $adminPwd

	#Create EDM Support users into the EDM DB
	Invoke-Sqlcmd -InputFile $supportUsersScript -ServerInstance $serverName -Database $edmDbName -Username $adminUser -Password $adminPwd
	
	#Create EDM Service users into the EDM DB
	Invoke-Sqlcmd -InputFile $serviceUsersScript -ServerInstance $serverName -Database $edmDbName -Username $adminUser -Password $adminPwd
	
	#Create additional tables in EDM DB used by housekeeping packages
	Invoke-Sqlcmd -InputFile $docsDboTblScript -ServerInstance $serverName -Database $edmDbName -Username $adminUser -Password $adminPwd
	
	#Create SP for rebuilding indexes
	Invoke-Sqlcmd -InputFile $indexRebuildScript -ServerInstance $serverName -Database $edmDbName -Username $adminUser -Password $adminPwd
	
	#Create SQL Job for optimising indexes
	Invoke-Sqlcmd -InputFile $indexOptimiseScript -ServerInstance $serverName -Database $edmDbName -Username $adminUser -Password $adminPwd
	
	#Apply license to the EDM DB
	Invoke-Sqlcmd -InputFile $edmLicense -ServerInstance $serverName -Database $edmDbName -Username $adminUser -Password $adminPwd
	
	#Create admin DB
	Invoke-Sqlcmd -InputFile $adminDBScript -ServerInstance $serverName -Database $adminDbName -Username $adminUser -Password $adminPwd
  EOH
end