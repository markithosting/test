Chef::Log.info("******Upgrade EDM Web UI******")

chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Stop the WebSite and AppPool
powershell_script 'Stop WebSite and AppPool' do
  code <<-EOH
	# Stop WebSite and AppPool
	Import-Module WebAdministration
	Stop-Website EDM
	Stop-WebAppPool EDM
	EOH
end

# Delete old Backups
powershell_script 'Delete Old Backups' do
  code <<-EOH
	# Check for older backups
	$oldBackups = Get-ChildItem -Path "D:\\Inetpub\\EDM" | Where-Object { $_.Name -like "*old*" }
	
	# if found, delete them
	foreach ($backup in $oldBackups) {
		Remove-Item $backup.FullName -Recurse
	}
	EOH
end

# Backup Web UI and Web Service
powershell_script 'Backup Websites' do
  code <<-EOH
	# Web UI and Service paths
	$uiPath = "D:\\inetpub\\EDM"
	$uiBackup = "D:\\inetpub\\EDM Old"
	$svcPath = "D:\\inetpub\\Markit EDM Web Service"
	$svcBackup = "D:\\inetpub\\Markit EDM Web Service Old"

	# Backup current Web UI and Web Service directories
	Move-Item $uiPath $uiBackup
	Move-Item $svcPath $svcBackup
	EOH
end

# Delete old Setup CDs and DPPlugins
powershell_script 'Clean Up Old Installers' do
  code <<-EOH
	# Check for Setup CDs and DPPlugIns for older versions
	$oldObjects = Get-ChildItem -Path "D:\\software\\EDM" | Where-Object { $_.Name -like "*Setup*" -or "*plugin*" }
	
	# if found, delete them
	foreach ($object in $oldObjects) {
		Remove-Item $object.FullName -Recurse
	}
	EOH
end

# Download EDM Setup CD
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'edm-installers',
                     key: 'latest/MarkitEDMSetupCD.zip',
                     response_target: 'd:\software\EDM\MarkitEDMSetupCD.zip')
                    
  end
  action :run
  not_if {::File.exists?('D:\Software\EDM\MarkitEDMSetupCD.zip')}
end

# Unzip the EDM setup CD
windows_zipfile 'D:\Software\EDM' do
  source 'd:\software\EDM\MarkitEDMSetupCD.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\EDM\Setup CD\Markit EDM Typical\MarkitEDMSetup.exe')}
end

# Rename the extracted setup CD directory to remove version number to keep standard
powershell_script 'Rename EDM Directory' do
  code 'Start-Sleep -Seconds 60
	$setupCD = Get-ChildItem -Path D:\\software -Recurse | Where-Object Name -Like "Setup CD*"
	Move-Item $SetupCD.FullName "D:\\Software\\EDM\\Setup CD"'
  not_if "Test-Path 'D:\\Software\\EDM\\Setup CD'"
end

# Deploy New Websites
powershell_script 'Deploy New Websites' do
  code <<-EOH
	# Copy new Web UI and Web Service from the Setup CD
	Copy-Item 'D:\\Software\\edm\\Setup CD\\Web UI' 'D:\\inetpub' -Recurse
	Move-Item 'D:\\inetpub\\Web UI' 'D:\\inetpub\\EDM' 
	Copy-Item 'D:\\Software\\EDM\\Setup CD\\Markit EDM Web Service' 'D:\\inetpub' -Recurse
	
	# Copy Web UI Web.config details
	
	# Web.config backup location
	$oldUiWebConfig = 'D:\\inetpub\\edm old\\Web.config'
	# new web.config location
	$newUiWebConfig = 'D:\\inetpub\\edm\\Web.config'
	# Store each as a variable
	$oldUiSettings = [xml](Get-Content $oldUiWebConfig)
	$newUiSettings = [xml](Get-Content $newUiWebConfig)

	# Load Server name from backup file
	$oldUiServerNode = $oldUiSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Server'}
	$uiServer = $oldUiServerNode.value
	# Set the Server name in the new file
	$uiServerNode = $newUiSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Server'}
	$uiServerNode.Value = $uiServer

	# Load db name from backup file
	$oldUiDbNode = $oldUiSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Database'}
	$uiDatabase = $oldUiDbNode.value
	# Set the db name in the new file
	$uiDbNode = $newUiSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Database'}
	$uiDbNode.Value = $uiDatabase

	# Load username from backup file
	$oldUiUserNode = $oldUiSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Username'}
	$uiUsername = $oldUiUserNode.value
	# Set the username in the new file
	$uiUserNode = $newUiSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Username'}
	$uiUserNode.Value = $uiUsername

	# Load password name from backup file
	$oldUiPasswordNode = $oldUiSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Password'}
	$uiPassword = $oldUiPasswordNode.value
	# Set the password in the new file
	$uiPasswordNode = $newUiSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Password'}
	$uiPasswordNode.Value = $uiPassword

	# Save the settings
	$newUiSettings.Save($newUiWebConfig) -as [xml]
	
	# Copy Web Svc Web.config details
	
	# Web.config backup location
	$oldSvcWebConfig = 'D:\\inetpub\\Markit EDM Web Service Old\\Web.config'
	# new web.config location
	$newSvcWebConfig = 'D:\\inetpub\\Markit EDM Web Service\\Web.config'
	# Store each as a variable
	$oldSvcSettings = [xml](Get-Content $oldSvcWebConfig)
	$newSvcSettings = [xml](Get-Content $newSvcWebConfig)

	# Load Server name from backup file
	$oldSvcServerNode = $oldSvcSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Server'}
	$SvcServer = $oldSvcServerNode.value
	# Set the Server name in the new file
	$SvcServerNode = $newSvcSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Server'}
	$SvcServerNode.Value = $SvcServer

	# Load db name from backup file
	$oldSvcDbNode = $oldSvcSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Database'}
	$SvcDatabase = $oldSvcDbNode.value
	# Set the db name in the new file
	$SvcDbNode = $newSvcSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Database'}
	$SvcDbNode.Value = $SvcDatabase

	# Load username from backup file
	$oldSvcUserNode = $oldSvcSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Username'}
	$SvcUsername = $oldSvcUserNode.value
	# Set the username in the new file
	$SvcUserNode = $newSvcSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Username'}
	$SvcUserNode.Value = $SvcUsername

	# Load password name from backup file
	$oldSvcPasswordNode = $oldSvcSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Password'}
	$SvcPassword = $oldSvcPasswordNode.value
	# Set the password in the new file
	$SvcPasswordNode = $newSvcSettings.configuration.appSettings.add | Where-Object {$_.Key -eq 'Password'}
	$SvcPasswordNode.Value = $SvcPassword

	# Save the settings
	$newSvcSettings.Save($newSvcWebConfig) -as [xml]
	EOH
end

# Start the WebSite and AppPool
powershell_script 'Start WebSite and AppPool' do
  code <<-EOH
	# Start WebSite and AppPool
	Start-Website EDM
	Start-WebAppPool EDM
	EOH
end