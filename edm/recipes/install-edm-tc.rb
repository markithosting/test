Chef::Log.info("******Install EDM Thick Client******")

# Use AWS SDK for S3 Object downloads
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Create directories for EDM operation
directory 'D:\Data\In' do
  recursive true
  action :create
end

directory 'D:\Data\Out' do
  action :create
end

directory 'D:\Software\EDM' do
  recursive true
  action :create
end

# Share the above EDM directories and assign permissions
powershell_script 'Create Data Share' do
  code <<-EOH
	$shareExists = Test-Path -Path \\\localhost\\Data

	If ($shareExists -eq $_.true) {
		$domainUsers = "domain users"
		New-SmbShare Data -Path d:\\data -FullAccess $domainUsers	
	}

	Else {
		Write-Host "Share already exists"
	}
	EOH
end

# Download EDM Setup CD
powershell_script 'Download EDM Setup CD' do
  code <<-EOH
	# Declare varibles
	$edmVersion = '#{node['software']['edm']['version']}'
	$edmVersionExists = Test-Path variable:\edmVersion
	$bucket = "edm-installers"
	$region = "us-east-1"
	$s3Key = "latest/MarkitEDMSetupCD.zip"
	$customS3Key = $edmVersion + "/MarkitEDMSetupCD.zip"
	$destination = "D:\\Software\\EDM\\MarkitEDMSetupCD.zip"

	# Download correct EDM version depending on value for Custom JSON in OpsWorks
	
	# If EDM version variable exists but is null then download the latest version
	If (!$edmVersion) {
		Read-S3Object -BucketName $bucket -Region $region -Key $s3Key -File $destination
	}

	# If EDM version variable exists then download the version specified
	ElseIf ($edmVersionExist -eq $_.true) {
		Read-S3Object -BucketName $bucket -Region $region -Key $customS3Key -File $destination
	}

	# In any other case, download the latest version of EDM
	Else {
		Read-S3Object -BucketName $bucket -Region $region -Key $s3Key -File $destination
	}
	EOH
  not_if {::File.exists?('D:\Software\EDM\MarkitEDMSetupCD.zip')}
end

# Unzip the EDM setup CD
windows_zipfile 'D:\Software\EDM' do
  source 'd:\software\EDM\MarkitEDMSetupCD.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\EDM\Setup CD\Markit EDM Typical\MarkitEDMSetup.exe')}
end

#Rename the extracted setup CD directory to remove version number to keep standard
powershell_script 'Rename EDM Directory' do
  code 'Start-Sleep -Seconds 60
	$setupCD = Get-ChildItem -Path D:\\software -Recurse | Where-Object Name -Like "Setup CD*"
	Move-Item $SetupCD.FullName "D:\\Software\\EDM\\Setup CD"'
	not_if "Test-Path 'D:\\Software\\EDM\\Setup CD'"
end

# Install EDM silently to a custom path, removing the version number to keep standard
windows_package 'Markit EDM Thick Client' do
  source 'D:\Software\EDM\Setup CD\Markit EDM Typical\MarkitEDMSetup.exe'
  options '/S /D=C:\Program Files\Markit Group\Markit EDM'
  installer_type :custom
  action :install
  retries 3
  retry_delay 120
  not_if {::File.exists?('C:\Program Files\Markit Group\Markit EDM\CADIS.exe')}
end