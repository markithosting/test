Chef::Log.info("******Restore Database******")

# Use AWS SDK for S3 Object downloads
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Create SQL Scripts local directory
directory 'D:\Software\SQLScripts' do
  recursive true
  action :create
end

# Download SQL Scripts
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'SQLScripts.zip',
                     response_target: 'd:\software\SQLScripts.zip')
                    
  end
  action :run
  not_if {::File.exists?('D:\Software\SQLScripts.zip')}
end

# Unzip SQL Scripts
windows_zipfile 'd:\software\SQLScripts' do
  source 'D:\Software\SQLScripts.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\SQLScripts\serviceusers.sql')}
end

powershell_script 'Export Existing DB Parameters' do
  code <<-EOH
 	# Declare variables
	$server = '#{node['dbdetails']['endpoint']}'
	$adminUser = '#{node['dbdetails']['adminUser']}'
	$adminPwd = '#{node['dbdetails']['adminPwd']}'
	$password = '#{node['dbdetails']['sqlPwd']}'
	$edmPath = "C:\\Program Files\\Markit Group\\Markit EDM\\CADISImportExport.exe"
	$dbParamsFile = "D:\\Software\\EDM\\DBParamatersBackup.xml"
	$grantsystemaccess = "D:\\Software\\SQLScripts\\grantsystemaccess.sql"
	$revokesystemaccess = "D:\\Software\\SQLScripts\\revokesystemaccess.sql"
	$now = (Get-Date).ToString("MM-dd-yyyy_hh-mm-ss")
  
	# Set the DB name and username based on the environment, determined by computer name
	If ($env:COMPUTERNAME -like "*dv*") {
		$dbName = "EDM_DEV"
		$username = "EDM_DEV_SERVICE"
	}
	ElseIf ($env:COMPUTERNAME -like "*ut*") {
		$dbName = "EDM_UAT"
		$username = "EDM_UAT_SERVICE"
	}
	ElseIf ($env:COMPUTERNAME -like "*pd*") {
		$dbName = "EDM_PROD"
		$username = "EDM_PROD_SERVICE"
	}
	Else {
		write-host "Unable to determine the EDM DB and service account name based on the computer name"
	}
  
	# Read scripts for domain and replace with userdomain for this customer environment
	(Get-Content $grantsystemaccess) -replace 'domain', $env:USERDOMAIN | Set-Content $grantsystemaccess
	(Get-Content $revokesystemaccess) -replace 'domain', $env:USERDOMAIN | Set-Content $revokesystemaccess

	# Grant temporary access for SYSTEM user to export DB parameters
	Invoke-Sqlcmd -InputFile $grantsystemaccess -ServerInstance $server -Database $dbName -Username $adminUser -Password $adminPwd

	$arguments = "/action:Export /type:DBParameters /filename:$dbParamsFile /server:$server /db:$dbName /integrated:No /user:$username /password:$password"
  
	Start-Process -FilePath $edmpath -ArgumentList $arguments -RedirectStandardOutput "C:\\Program Files\\Markit Group\\MarkitEDM Temporary Files\\Temp\\DBParametersExport-$now.txt"
	
	# Wait for package import to complete before removing permission
	Start-Sleep -Seconds 20

	# Revoke temporary access
	#Invoke-Sqlcmd -InputFile $revokesystemaccess -ServerInstance $server -Database $dbName -Username $adminUser -Password $adminPwd
	EOH
end

powershell_script 'Configure DB Restore Script' do
  code <<-EOH
	# Set the DB name based on the environment, determined by computer name
	If ($env:COMPUTERNAME -like "*dv*") {
    		$edmDbName = "EDM_DEV"
  	}
  	ElseIf ($env:COMPUTERNAME -like "*ut*") {
    		$edmDbName = "EDM_UAT"
  	}
  	ElseIf ($env:COMPUTERNAME -like "*pd*") {
    		$edmDbName = "EDM_PROD"
  	}
	Else {
		write-host "Unable to determine EDM DB Name"
	}
	
	# Get the availability zone to determine which S3 bucket to restore from
	$metadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/placement/availability-zone" -UseBasicParsing
	$az = $metadata.Content
	
	# Get the domain name and backup file names to prepare the S3 bucket to restore from
	$domain = $env:USERDOMAIN + ".com"
	$edmDbBakName = $edmDbName + ".bak"

	# Set the S3 ARNs to backup to depending on the instance region
	If ($az -like "us*") {
		$s3ArnEDM = "arn:aws:s3:::" + "edm-us/" + $domain + "/rds-backups/" + $edmDbBakName
		$kmsArn = "arn:aws:kms:us-east-1:829846211589:key/001706b6-b48a-4230-a077-b0a30b0a6a62"
	}

	ElseIf ($az -like "eu*") {
		$s3ArnEDM = "arn:aws:s3:::" + "edm-us/" + $domain + "/rds-backups/" + $edmDbBakName
		$kmsArn = "arn:aws:kms:eu-west-1:829846211589:key/17699831-ad50-45d6-ae68-32bbf29e7484"
	}
	
	# Create the SQL script to drop the existing database and perform the restore
	$EdmDbRestContent = "USE [master] `
	GO `
	ALTER DATABASE [" + $edmDbName + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE `
	GO `
	DROP DATABASE [" + $edmDbName + "] `
	GO `
	`
	exec msdb.dbo.rds_restore_database `
        @restore_db_name='" + $edmDbName + "', `
        @s3_arn_to_restore_from='" + $s3ArnEDM + "', `
        @kms_master_key_arn='"+ $kmsArn + "';"
	
	$dbRestoreScript = "D:\\Software\\SQLScripts\\restoreEdmDb.sql"
	New-Item $dbRestoreScript -type file -force -value $EdmDbRestContent
  EOH
end

powershell_script 'Restore Database' do
  code <<-EOH
  	# Declare variables
	$server = '#{node['dbdetails']['endpoint']}'
	$adminUser = '#{node['dbdetails']['adminUser']}'
	$adminPwd = '#{node['dbdetails']['adminPwd']}'
	$dbRestoreScript = "D:\\Software\\SQLScripts\\restoreEdmDb.sql"
	
	#Run SQL Restore Script against the DB
	Invoke-Sqlcmd -InputFile $dbRestoreScript -ServerInstance $server -Database 'master' -Username $adminUser -Password $adminPwd
  EOH
end