Chef::Log.info("******Backup Database******")

powershell_script 'Configure Scripts' do
  code <<-EOH
	# Set the DB name based on the environment, determined by computer name
	If ($env:COMPUTERNAME -like "*dv*") {
    		$edmDbName = "EDM_DEV"
  	}
  	ElseIf ($env:COMPUTERNAME -like "*ut*") {
    		$edmDbName = "EDM_UAT"
  	}
  	ElseIf ($env:COMPUTERNAME -like "*pd*") {
    		$edmDbName = "EDM_PROD"
  	}
	
	# Get the availability zone to determine which S3 bucket to backup to
	$metadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/placement/availability-zone" -UseBasicParsing
	$az = $metadata.Content
	
	# Get the domain name and backup file names to prepare the S3 bucket to backup to
	$domain = $env:USERDOMAIN + ".com"
	$edmDbBakName = $edmDbName + ".bak"

	# Set the S3 ARNs to backup to depending on the instance region
	If ($az -like "us*") {
		$s3ArnEDM = "arn:aws:s3:::" + "edm-us/" + $domain + "/rds-backups/" + $edmDbBakName
		$kmsArn = "arn:aws:kms:us-east-1:829846211589:key/001706b6-b48a-4230-a077-b0a30b0a6a62"
	}

	ElseIf ($az -like "eu*") {
		$s3ArnEDM = "arn:aws:s3:::" + "edm-us/" + $domain + "/rds-backups/" + $edmDbBakName
		$kmsArn = "arn:aws:kms:eu-west-1:829846211589:key/17699831-ad50-45d6-ae68-32bbf29e7484"
	}
	
	# Create the SQL script to perform the backup
	$EdmDbBakContent = "exec msdb.dbo.rds_backup_database `
        @source_db_name='" + $edmDbName + "', `
        @s3_arn_to_backup_to='" + $s3ArnEDM + "', `
        @kms_master_key_arn='"+ $kmsArn + "', `
        @overwrite_S3_backup_file=1;"
	
    $dbBackupScript = "D:\\Software\\SQLScripts\\backupEdmDb.sql"
    New-Item $dbBackupScript -type file -force -value $EdmDbBakContent
  EOH
end

powershell_script 'Create Backup' do
  code <<-EOH
	#Function to import SQLPS module taken from http:///mikefrobbins.com//2015//06/25//function-to-import-the-sqlps-powershell-module-or-snap-in
	function Import-SqlModule {
    		[CmdletBinding()]
   		param ()
    		if (-not(Get-Module -Name SQLPS) -and (-not(Get-PSSnapin -Name SqlServerCmdletSnapin100, SqlServerProviderSnapin100 -ErrorAction SilentlyContinue))) {
    		Write-Verbose -Message 'SQLPS PowerShell module or snapin not currently loaded'
 
        		if (Get-Module -Name SQLPS -ListAvailable) {
        		Write-Verbose -Message 'SQLPS PowerShell module found'
 
            			Push-Location
            			Write-Verbose -Message "Storing the current location: '$((Get-Location).Path)'"
 
            			if ((Get-ExecutionPolicy) -ne 'Restricted') {
                		Import-Module -Name SQLPS -DisableNameChecking -Verbose:$false
                		Write-Verbose -Message 'SQLPS PowerShell module successfully imported'
            			}
            			else{
                		Write-Warning -Message 'The SQLPS PowerShell module cannot be loaded with an execution policy of restricted'
            			}
            
            			Pop-Location
            			Write-Verbose -Message "Changing current location to previously stored location: '$((Get-Location).Path)'"
        		}
        		elseif (Get-PSSnapin -Name SqlServerCmdletSnapin100, SqlServerProviderSnapin100 -Registered -ErrorAction SilentlyContinue) {
        		Write-Verbose -Message 'SQL PowerShell snapin found'
 
            			Add-PSSnapin -Name SqlServerCmdletSnapin100, SqlServerProviderSnapin100
            			Write-Verbose -Message 'SQL PowerShell snapin successfully added'
 
            			[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.Smo') | Out-Null
            			Write-Verbose -Message 'SQL Server Management Objects .NET assembly successfully loaded'
        		}
        		else {
            			Write-Warning -Message 'SQLPS PowerShell module or snapin not found'
        		}
    		}
    		else {
        		Write-Verbose -Message 'SQL PowerShell module or snapin already loaded'
    		}
 
	}

	#Import the previously created module
	Import-SqlModule       	

	[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.SqlServer.SMO') | out-null
    
	#Declare all variables
	$serverName = '#{node['dbdetails']['endpoint']}'
	$adminUser = '#{node['dbdetails']['adminUser']}'
	$adminPwd = '#{node['dbdetails']['adminPwd']}'
	$dbBackupScript = "D:\\Software\\SQLScripts\\backupEdmDb.sql"
	$server = new-object ('Microsoft.SqlServer.Management.Smo.Server') $serverName
	$server.ConnectionContext.LoginSecure=$false;
	$server.ConnectionContext.set_Login($adminUser)
	$securePassword = ConvertTo-SecureString $adminPwd -AsPlainText -Force
	$server.ConnectionContext.set_SecurePassword($securePassword)
	$server.ConnectionContext.ApplicationName="SQLDeploymentScript"

	#Run SQL Backup Script against the DB
	Invoke-Sqlcmd -InputFile $dbBackupScript -ServerInstance $serverName -Database 'master' -Username $adminUser -Password $adminPwd
  EOH
end