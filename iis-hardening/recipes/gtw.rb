Chef::Log.info("******IIS Hardening - RD Gateway******")

# Disable insecure protocols - PCT 1.0, SSL 2.0, SSL 3.0
registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\PCT 1.0\Server' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  },
  {
    :name => 'DisabledByDefault',
    :type => :dword,
    :data => 1
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Server' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  },
  {
    :name => 'DisabledByDefault',
    :type => :dword,
    :data => 1
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Server' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  },
  {
    :name => 'DisabledByDefault',
    :type => :dword,
    :data => 1
  }]
  action :create
end

# Disable insecure ciphers - NULL, DES, RC2 and RC4
registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\DES 56/56' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\NULL' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 128/128' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 40/128' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC2 56/128' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 128/128' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 40/128' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 56/128' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 64/128' do
  recursive true
  values [{
    :name => 'Enabled',
    :type => :dword,
    :data => 0
  }]
  action :create
end