Chef::Log.info("******Download and Deploy Packages******")

# Use AWS SDK for S3 Object downloads
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Create SQL Scripts local directory
directory 'D:\Software\SQLScripts' do
  recursive true
  action :create
end

# Create EDM packages local directory
directory 'D:\Software\EDM\Packages' do
  recursive true
  action :create
end

# Download SQL Scripts
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'SQLScripts.zip',
                     response_target: 'd:\software\SQLScripts.zip')
                    
  end
  action :run
  not_if {::File.exists?('D:\Software\SQLScripts.zip')}
end

# Unzip SQL Scripts
windows_zipfile 'd:\software\SQLScripts' do
  source 'D:\Software\SQLScripts.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\SQLScripts\serviceusers.sql')}
end

# Download packages
powershell_script 'Download Packages' do
  code <<-EOH
  # Define bucket details
  $region = "us-east-1"
  $bucket = "edm-packages"

  # Get the characters 3 and 4 from the computer name, this is the prefix name in the bucket
  $prefix = $env:computername.substring(2,2)
  $bucketName = Join-Path $bucket $prefix

  # List all packages in the bucket
  $packages = Get-S3Object -BucketName $bucket -KeyPrefix $prefix -Region $region | Where-Object { $_.Key -like "*.xml" }

  # Download each Package
  foreach ($package in $packages) {
    $localPackages = "d:\\Software\\EDM\\Packages"
    $keyPath = $package.Key
    $key = $keyPath.Substring(2)
    $localPath = Join-Path $localPackages $key
    Read-S3Object -BucketName $bucket -Region $region -Key $keyPath -File $localPath
  }
  EOH
end

powershell_script 'Deploy Packages' do
  code <<-EOH
  # Declare variables
  $server = '#{node['dbdetails']['endpoint']}'
  $adminUser = '#{node['dbdetails']['adminUser']}'
  $adminPwd = '#{node['dbdetails']['adminPwd']}'
  $password = '#{node['dbdetails']['sqlPwd']}'
  $edmPath = "C:\\Program Files\\Markit Group\\Markit EDM\\CADISImportExport.exe"
  $packageList = "D:\\Software\\EDM\\Packages"
  $now = (Get-Date).ToString("MM-dd-yyyy_hh-mm-ss")
  $grantsystemaccess = "D:\\Software\\SQLScripts\\grantsystemaccess.sql"
  $revokesystemaccess = "D:\\Software\\SQLScripts\\revokesystemaccess.sql"

  # Set the DB name and username based on the environment, determined by computer name
  If ($env:COMPUTERNAME -like "*dv*") {
      $dbName = "EDM_DEV"
      $username = "EDM_DEV_SERVICE"
  }
  ElseIf ($env:COMPUTERNAME -like "*ut*") {
      $dbName = "EDM_UAT"
      $username = "EDM_UAT_SERVICE"
  }
  ElseIf ($env:COMPUTERNAME -like "*pd*") {
      $dbName = "EDM_PROD"
      $username = "EDM_PROD_SERVICE"
      }

  # Read scripts for domain and dbname and replace with userdomain and dbname for this customer environment
  (Get-Content $grantsystemaccess) -replace 'domain', $env:USERDOMAIN | Set-Content $grantsystemaccess
  (Get-Content $revokesystemaccess) -replace 'domain', $env:USERDOMAIN | Set-Content $revokesystemaccess

  # Grant temporary access for SYSTEM user to import packages
  Invoke-Sqlcmd -InputFile $grantsystemaccess -ServerInstance $server -Database $dbName -Username $adminUser -Password $adminPwd

  # List all packages
  $packages = Get-ChildItem -path $packageList -filter "*.xml"

  foreach ($package in $packages) {
      # Get full package name including path
      $packagePath = $package.FullName

      # Package Import Export command line arguments
      $arguments = "/action:Import /allconsolegroups:yes /isazure:no /server:$server /db:$dbName /integrated:No /user:$username /password:$password /type:Package /filename:$packagePath"
    
      # Run the import and log the standard output
      Start-Process -FilePath $edmpath -ArgumentList $arguments -RedirectStandardOutput C:\\temp\\$package$now.txt
    
      # Move the file into the archive directory for reporting
      Move-Item $packagePath "D:\\Data\\Packages\\Automate\\Archive\\$package$now"
  }

  # Wait for package import to complete before removing permission
  Start-Sleep -Seconds 60

  # Revoke temporary access
  #Invoke-Sqlcmd -InputFile $revokesystemaccess -ServerInstance $server -Database $dbName -Username $adminUser -Password $adminPwd
  EOH
end