Chef::Log.info("******Install 7-Zip******")

directory 'c:\programdata\7zip' do
  recursive true
  action :create
end

windows_package '7-Zip' do
  source 'http://www.7-zip.org/a/7z1602-x64.msi'
  options '/L*v c:\programdata\7zip\7zip_install.log /qn'
  installer_type :msi
  action :install
  retries 3
  retry_delay 120
  not_if {::File.exists?('C:\Program Files\7-Zip\7z.exe')}
end


