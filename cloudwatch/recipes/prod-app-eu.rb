Chef::Log.info("******Configure CloudWatch******")

# Use AWS SDK for S3 Object downloads
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Rename the current CloudWatch Config file
powershell_script 'Rename File' do
  code 'Move-Item "C:\\Program Files\\Amazon\\Ec2ConfigService\\Settings\\AWS.EC2.Windows.CloudWatch.json" "C:\\Program Files\\Amazon\\Ec2ConfigService\\Settings\\AWS.EC2.Windows.CloudWatch.bak" -Force'
end

# Download EDM Setup CD
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'AWS.EC2.Windows.CloudWatchEU.json',
                     response_target: 'C:\Program Files\Amazon\Ec2ConfigService\Settings\AWS.EC2.Windows.CloudWatch.json')
                    
  end
  action :run
  not_if {::File.exists?('C:\Program Files\Amazon\Ec2ConfigService\Settings\AWS.EC2.Windows.CloudWatch.json')}
end

# Restart the EC2Config service
windows_service 'Ec2Config' do
  action :restart
end