Chef::Log.info("******Group Memberships******")

# Permit RDP based on group
powershell_script 'RDP Users' do
  code	<<-EOH
  # Work out the group depending on the environment and store in a variable
  If ($env:COMPUTERNAME -like "*dv*") {
    $clientusers = "Client Development"
    $markitusers = "Markit Development"
  }
  ElseIf ($env:COMPUTERNAME -like "*ut*") {
    $clientusers = "Client UAT"
	$markitusers = "Markit UAT"
  }
  # Specify the target group
  $RDPGroup = [ADSI]"WinNT://$env:COMPUTERNAME/Remote Desktop Users,group"
  # Specify the groups to be added to the target
  $clientgroup = [ADSI]"WinNT://$env:USERDOMAIN/$clientusers,group"
  $markitgroup = [ADSI]"WinNT://$env:USERDOMAIN/$markitusers,group"
  # Add groups to target
  $RDPGroup.Add($clientgroup.Path)
  $RDPGroup.Add($markitgroup.Path)
  EOH
end