Chef::Log.info("******Add Domain Users to Local Admins******")

# Add domain users to local administrators group
domain_users = node['domain'] + '\\Domain Users'

group 'Administrators' do
  members domain_users
  append true
  action :modify
end