Chef::Log.info("******Install Jenkins******")

directory 'c:\programdata\jenkins' do
  recursive true
  action :create
end

directory 'd:\software' do
  action :create
end

windows_zipfile 'd:\software\jenkins' do
  source 'http://mirrors.jenkins-ci.org/windows/jenkins-1.643.zip'
  action :unzip
  not_if {::File.exists?('d:\software\jenkins\jenkins.msi')}
end

windows_package 'Jenkins' do
  source 'd:\software\jenkins\jenkins.msi'
  options '/L*v c:\programdata\jenkins\jenkins.log /qn'
  installer_type :msi
  action :install
  retries 3
  retry_delay 120
  not_if {::File.exists?('C:\Program Files (x86)\Jenkins\jenkins.exe')}
end

# Install the Naginator Plugin
powershell_script 'Install Plugin - Naginator' do
  code <<-EOH
	Start-Sleep -seconds 120
	$Url = "http://updates.jenkins-ci.org/download/plugins/naginator/1.17.2/naginator.hpi"
	$LocalPath = "C:\\Program Files (x86)\\Jenkins\\plugins\\naginator.hpi"
	$Wget = New-Object System.Net.WebClient
	$Wget.DownloadFileAsync($Url, $LocalPath)
	Start-Sleep -Seconds 5
	Restart-Service -Name Jenkins
  EOH
end

# Set service logon details and dir permissions
# Chef run_as_user property on windows_service does not run on Chef 12.2 so this code is a workaround
powershell_script 'Jenkins Service' do
  code	<<-EOH
  
  # Extract the domain name from AWS metadata
  $hostname = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/hostname" -UseBasicParsing
  $fqdn = $hostname.Content
  $domain = $fqdn.Split('.')[+1]
  
  # Work out the username depending on the environment and store in a variable
  If ($env:COMPUTERNAME -like "*dv*") {
  $username = $domain+"\\edm_dev_service"
  }
  ElseIf ($env:COMPUTERNAME -like "*ut*") {
    $username = $domain+"\\edm_uat_service"
  }
  ElseIf ($env:COMPUTERNAME -like "*pd*") {
    $username = $domain+"\\edm_prod_service"
  }
  c:\\chef\\cookbooks\\files\\LogOnAsService.ps1 $username
  $password = '#{node['dbdetails']['sqlPwd']}'
  $svc = gwmi win32_service -filter "name='Jenkins'"
  $svc | Invoke-WmiMethod -Name Change -ArgumentList @($null,$null,$null,$null,$null,$null,$null,$null,$null,$username,$password)

  # Give the service account Modify permission on the install dir
  $permission = $username,"Modify","ContainerInherit,ObjectInherit","None","Allow"
  $acl = Get-Acl "C:\\Program Files (x86)\\Jenkins"
  $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule $permission
  $acl.SetAccessRule($accessRule)
  $acl | Set-Acl "C:\\Program Files (x86)\\Jenkins"
  EOH
end