# Download EDM Setup CD
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'edm-installers',
                     key: 'latest/MarkitEDMSetupCD.zip',
                     response_target: 'd:\software\EDM\MarkitEDMSetupCD.zip')
                    
  end
  action :run
  not_if {::File.exists?('D:\Software\EDM\MarkitEDMSetupCD.zip')}
end

# Unzip the EDM setup CD
windows_zipfile 'D:\Software\EDM' do
  source 'd:\software\EDM\MarkitEDMSetupCD.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\EDM\Setup CD\Markit EDM Typical\MarkitEDMSetup.exe')}
end

#Rename the extracted setup CD directory to remove version number to keep standard
powershell_script 'Rename EDM Directory' do
  code 'Start-Sleep -Seconds 60
	$setupCD = Get-ChildItem -Path D:\\software -Recurse | Where-Object Name -Like "Setup CD*"
	Move-Item $SetupCD.FullName "D:\\Software\\EDM\\Setup CD"'
	not_if "Test-Path 'D:\\Software\\EDM\\Setup CD'"
end

# Download Standard SQL Scripts
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'SQLScripts.zip',
                     response_target: 'd:\software\SQLScripts.zip')
                    
  end
  action :run
  not_if {::File.exists?('D:\Software\SQLScripts.zip')}
end

# Download Upgrade SQL Scripts
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'EDMUpgradeScripts.zip',
                     response_target: 'd:\software\EDMUpgradeScripts.zip')
                    
  end
  action :run
  not_if {::File.exists?('D:\Software\EDMUpgradeScripts.zip')}
end

# Unzip SQL Scripts
windows_zipfile 'd:\software\SQLScripts' do
  source 'D:\Software\SQLScripts.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\SQLScripts\serviceusers.sql')}
end

# Unzip Upgrade SQL Scripts
windows_zipfile 'd:\software\SQLScripts' do
  source 'D:\Software\EDMUpgradeScripts.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\SQLScripts\releaseallprocesslocks.sql')}
end

powershell_script 'Database Upgrade' do
  code <<-EOH
    $grantsystemaccess = "D:\\Software\\SQLScripts\\grantsystemaccess.sql"
	$revokesystemaccess = "D:\\Software\\SQLScripts\\revokesystemaccess.sql"
    $modifyupgradedbname = "D:\\Software\\SQLScripts\\modifyupgradedbname.sql"
    $upgradeserviceuser = "D:\\Software\\SQLScripts\\upgradeserviceuser.sql"
    $releaseallprocesslocks = "D:\\Software\\SQLScripts\\releaseallprocesslocks.sql"

	$server = '#{node['dbdetails']['endpoint']}'
	$adminUser = '#{node['dbdetails']['adminPwd']}'
	$adminPwd = '#{node['dbdetails']['adminUser']}'	
	$edmPath = "D:\\Software\\EDM\\Setup CD\\DatabaseUpgradeWizard\\DatabaseUpgradeWizard.exe"
	$isintegrated = 'No'
	$database = 'EDM_UPGRADE'
    $serviceuser = 'EDM_UPGRADE_SERVICE'

	$now = (Get-Date).ToString("MM-dd-yyyy_hh-mm-ss")

	$arguments = "/server:$server /db:$database /integrated:$isintegrated /user:$serviceuser /password:$adminPwd"
    $dir = "D:\\upgradelogs"
    $logdir = "D:\\upgradelogs\\MarkitEDM Temporary Files\\DB Upgrade Logs"


# Override default EDM temp location to get the upgrade logs.
# https://supportlibrary.markit.com/display/EDM/Overriding+Settings+and+Temporary+Files+Directories

    $filePath = "D:\\Software\\EDM\\Setup CD\\DatabaseUpgradeWizard\\Config\\CustomFileLocations.config"
	$XmlWriter = New-Object System.XMl.XmlTextWriter($filePath,$Null)
	$xmlWriter.Formatting = "Indented"
	$xmlWriter.Indentation = "4"
	$xmlWriter.WriteStartDocument()
	$xmlWriter.WriteStartElement("CustomFileLocations")
	$xmlWriter.WriteElementString("TempRootLocation",$dir)
	$xmlWriter.WriteEndElement()
	$xmlWriter.WriteEndDocument()
	$xmlWriter.Flush()
	$xmlWriter.Close()



#Create upgrade logs output directory

New-Item D:\upgradelogs -type directory -force

New-Item D:\upgradelogsarchive -type directory -force


#Rename database from EDM_PROD to EDM_UPGRADE

Invoke-Sqlcmd -InputFile $modifyupgradedbname -ServerInstance $server -Database 'EDM_PROD' -Username $adminUser -Password $adminPwd

# Read scripts for domain and dbname and replace with userdomain and dbname for this customer environment
(Get-Content $grantsystemaccess) -replace 'domain', $env:USERDOMAIN | Set-Content $grantsystemaccess
(Get-Content $revokesystemaccess) -replace 'domain', $env:USERDOMAIN | Set-Content $revokesystemaccess

#Replace Password for Upgrate service user sql
(Get-Content $upgradeserviceuser) -replace 'PASSWORD_PH', $adminPwd | Set-Content $upgradeserviceuser

# Grant temporary access for SYSTEM user to complete upgrade
  Invoke-Sqlcmd -InputFile $grantsystemaccess -ServerInstance $server -Database $database -Username $adminUser -Password $adminPwd

# Grant temporary access for EDM_UPGRADE_SERVICE sql user to complete upgrade
  Invoke-Sqlcmd -InputFile $upgradeserviceuser -ServerInstance $server -Database $database -Username $adminUser -Password $adminPwd

# Release All Process Locks
  Invoke-Sqlcmd -InputFile $releaseallprocesslocks -ServerInstance $server -Database $database -Username $adminUser -Password $adminPwd

# Run the upgrade log the standard output
  Start-Process -FilePath $edmpath -ArgumentList $arguments -RedirectStandardOutput C:\\temp\\DatabaseUpgrade$now.txt

# Track the upgrade - Powershell will complete without the loop
Start-Sleep -s 60
$upgradelog = Get-ChildItem -Path $logdir | Sort-Object LastAccessTime -Descending | Select-Object -First 1
write-host $upgradelog
  Do {
    
   Start-Sleep -s 60
   $upgradecompleted = Select-String -Path $logdir\$upgradelog -Pattern 'Process upgrade completed'
   write-host "still running...."
    }
  While (!$upgradecompleted)
  write-host "upgrade completed"

# Cleanup Software folder to ensure new versions are downloaded each time the recipe is ran.
  Remove-Item D:\\Software\\* -recurse

  EOH
end
