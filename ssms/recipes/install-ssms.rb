Chef::Log.info("******Install SQL Server Management Studio******")

chef_gem "aws-sdk" do
  compile_time false
  action :install
end

directory 'd:\software' do
  action :create
end

ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'SSMS2014.zip',
                     response_target: 'd:\software\SSMS2014.zip')                    
  end
  action :run
  not_if {::File.exists?('d:\software\SSMS2014.zip')}
end

powershell_script 'Install Windows Features' do
  code 'Add-WindowsFeature NET-FrameWork-Features -IncludeAllSubFeature -restart'
  not_if "(Get-WindowsFeature -Name NET-FrameWork-Features).Installed"
end

windows_zipfile 'd:\software' do
  source 'd:\software\SSMS2014.zip'
  action :unzip
  not_if {::File.exists?('d:\software\SSMS2014\setup.exe')}
end

windows_package 'SQL Server Management Studio' do
  source 'd:\software\SSMS2014\setup.exe'
  options '/ACTION=INSTALL /QUIET /IAcceptSQLServerLicenseTerms=True /FEATURES=CONN,BC,SSMS'
  installer_type :custom
  action :install
  success_codes << 3010
  retries 5
  retry_delay 120
  not_if {::File.exists?('C:\Program Files (x86)\Microsoft SQL Server\120\Tools\Binn\ManagementStudio\Ssms.exe')}
end