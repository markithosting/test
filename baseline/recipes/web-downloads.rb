Chef::Log.info("******Downloading files from S3******")

chef_gem "aws-sdk" do
  compile_time false
  action :install
end

Chef::Log.info("******Creating local directories******")

directory 'd:\software' do
  action :create
end

directory 'c:\cfn' do
  action :create
end

directory 'c:\cfn\scripts' do
  action :create
end

Chef::Log.info("******Performing file download******")

ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'NewRelicServerMonitor_x64_3.3.5.0.msi',
                     response_target: 'd:\software\NewRelicServerMonitor_x64_3.3.5.0.msi')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'Markit_EDM_v10-5-0-1_Setup_CD.zip',
                     response_target: 'd:\software\Markit_EDM_v10-5-0-1_Setup_CD.zip')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'SSMS2014.zip',
                     response_target: 'd:\software\SSMS2014.zip')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'Cloudwatch-Log.ps1',
                     response_target: 'c:\cfn\scripts\Cloudwatch-Log.ps1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'AWS.EC2.Windows.CloudWatchEU.json',
                     response_target: 'c:\Program Files\Amazon\Ec2ConfigService\Settings\AWS.EC2.Windows.CloudWatch.json')
                    
  end
  action :run
end