Chef::Log.info("******Configuring Role: RDS Gateway******")

powershell_script 'Configure RDS-Gateway' do
  code <<-EOH
	#Import PS Module
	Import-Module remotedesktopservices
	
	#Variables to set FQDN, domain name and default group
	$ServerFQDN = "$env:computername.$env:userdnsdomain"
	$DomainNetBiosName = "$env:USERDOMAIN"
	$GroupName = "domain users"

	#Create default RD Gateway CAP and RAP policies
	new-item -Path "RDS:\\GatewayServer\\CAP" -Name Default-CAP -UserGroups "$GroupName@$DomainNetBiosName" -AuthMethod 1
	new-item -Path "RDS:\\GatewayServer\\RAP" -Name Default-RAP -UserGroups "$GroupName@$DomainNetBiosName" -ComputerGroupType 2
	
	#Restart the service
	Restart-Service tsgateway
	EOH
end

powershell_script 'Enable Password Reset' do
  code <<-EOH
	#Web config and backup locations
	$webConfig = "C:\\Windows\\Web\\RDWeb\\Pages\\Web.config"
	$backup = "C:\\Windows\\Web\\RDWeb\\Pages\\Web.config-backup"

	#Backup current web config file
	Copy-Item $webConfig -Destination $backup

	#Read web config file and enable set PasswordChangeEnabled key to true
	$doc = (Get-Content $webConfig) -replace 'add key="PasswordChangeEnabled" value="false" ', 'add key="PasswordChangeEnabled" value="true" ' -as [Xml]

	#Save the file
	$doc.Save($webConfig)
	EOH
end