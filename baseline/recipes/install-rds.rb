Chef::Log.info("******Deploying Role: RDS Session Host******")

powershell_script 'Install rd-sessionhost' do
  code 'Install-WindowsFeature RDS-RD-Server -IncludeManagementTools -IncludeAllSubfeature'
  not_if "(Get-WindowsFeature -Name RDS-RD-Server).Installed"
end