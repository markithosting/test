Chef::Log.info("******Disable Clipboard/Client Drive Mapping******")

# Disable clipboard and client drive mapping over RDP. To disable client drive mapping all 3 keys must be set. To disable the clipboard only then you can just use the fdisableClip key
registry_key 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows NT\Terminal Services' do
  recursive true
  values [{
    :name => 'fDisableCdm',
    :type => :dword,
    :data => 1
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' do
  recursive true
  values [{
    :name => 'fDisableCdm',
    :type => :dword,
    :data => 1
  }]
  action :create
end

registry_key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' do
  recursive true
  values [{
    :name => 'fDisableClip',
    :type => :dword,
    :data => 1
  }]
  action :create
end