Chef::Log.info("******Deploying Role: RDS Gateway and Web Access******")

# Install RD Gateway Role
powershell_script 'Install RDS-Gateway' do
  code 'Install-WindowsFeature RDS-Gateway -IncludeManagementTools -IncludeAllSubfeature'
  not_if '(Get-WindowsFeature -Name RDS-Gateway).Installed'
end

# Install RD Web Access Role
powershell_script 'Install RDS-Web-Access' do
  code 'Install-WindowsFeature RDS-Web-Access -IncludeManagementTools -IncludeAllSubfeature'
  not_if '(Get-WindowsFeature -Name RDS-Web-Access).Installed'
end

# Configure RD Gateway Role
powershell_script 'Configure RDS-Gateway' do
  code <<-EOH
	#Import PS Module
	Import-Module remotedesktopservices
	
	# Get domain name from AWS metadata
	$hostnameMetadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/hostname" -UseBasicParsing
    $fqdn = $hostnameMetadata.Content
    $domain = $fqdn.Split('.')[+1]
    $domainname = $domain + ".com"
    
    # Get hostname tag from EC2
    $instance = (Get-EC2Instance -Instance $instanceId -Region $region).Instances
    $tag = $instance.Tags | Where-Object { $_.Key -eq "opsworks:instance" }
    $hostname = $tag.Value

	# Group to be permitted access through the RD Gateway
	$GroupName = "domain users"

	#Create default RD Gateway CAP and RAP policies
	new-item -Path "RDS:\\GatewayServer\\CAP" -Name Default-CAP -UserGroups "$GroupName@$domain" -AuthMethod 1
	new-item -Path "RDS:\\GatewayServer\\RAP" -Name Default-RAP -UserGroups "$GroupName@$domain" -ComputerGroupType 2
	
	#Restart the service
	Restart-Service tsgateway
	EOH
end

# Enable password reset feature in RD Web Access
powershell_script 'Enable Password Reset' do
  code <<-EOH
	#Web config and backup locations
	$webConfig = "C:\\Windows\\Web\\RDWeb\\Pages\\Web.config"
	$backup = "C:\\Windows\\Web\\RDWeb\\Pages\\Web.config-backup"

	#Backup current web config file
	Copy-Item $webConfig -Destination $backup

	#Read web config file and enable set PasswordChangeEnabled key to true
	$doc = (Get-Content $webConfig) -replace 'add key="PasswordChangeEnabled" value="false" ', 'add key="PasswordChangeEnabled" value="true" ' -as [Xml]

	#Save the file
	$doc.Save($webConfig)
	EOH
end

# Create Route53 A record
powershell_script 'Create Route 53 Record' do
  code <<-EOH
	# Zone name to check in Route 53
    $hostnameMetadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/hostname" -UseBasicParsing
    $fqdn = $hostnameMetadata.Content
    $domain = $fqdn.Split('.')[+1]
    $zonename = $domain + ".com."
    $domainname = $domain + ".com"

    # Get instance Id from metadata
    $instanceMetadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/instance-id" -UseBasicParsing
    $instanceId = $instanceMetadata.Content

    # Get region by trimming the last character off the AZ from metadata
    $azMetadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/placement/availability-zone" -UseBasicParsing
    $az = $azMetadata.Content
    $region = $az.Substring(0,$az.Length-1)

    # Get hostname tag from EC2
    $instance = (Get-EC2Instance -Instance $instanceId -Region $region).Instances
    $tag = $instance.Tags | Where-Object { $_.Key -eq "opsworks:instance" }
    $hostname = $tag.Value
	
    # Locate the zone name in Route 53
    $zone = Get-R53HostedZonesByName | Where-Object { $_.Name -EQ $zonename }
    # Select the ID and remove the first 12 characters to get the Hosted Zone ID
    $zone.Id
    $zoneId = $zone.Id.Substring(12)

    # Get the EIP to create the A record
    $metadata = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/public-ipv4" -UseBasicParsing
    $eip = $metadata.Content

    # Search for a Hosted Zone with matching Zone ID
    $domain = Get-R53ResourceRecordSet -HostedZoneId $zoneId
    # List all resource records in the zone
    $records = $domain.ResourceRecordSets.Name
    # FQDN to check for in Route 53
    $matchRecord = $hostname + "." + $zonename

    # Create a new resource record for the server if it does not already exist
    if ($records -eq $matchRecord) {
    	write-host "record already exists"
    }

    else {
    	# A record to be created
    	$aRecord = $hostname + "." + $domainname
    	
    	# Build up parameters to add
    	$record = New-Object Amazon.Route53.Model.Change
    	$record.Action = "CREATE"
    	$record.ResourceRecordSet = New-Object Amazon.Route53.Model.ResourceRecordSet
    	$record.ResourceRecordSet.Name = $aRecord
    	$record.ResourceRecordSet.Type = "A"
    	$record.ResourceRecordSet.TTL = 600
    	$record.ResourceRecordSet.ResourceRecords.Add(@{Value=$eip})

    	$params = @{
    	HostedZoneId=$zoneId
    	ChangeBatch_Comment="Create A record to resolve EIP to FQDN"
    	ChangeBatch_Change=$record
    	}

    # Create the A record in Route 53
    Edit-R53ResourceRecordSet @params
    }
  EOH
end