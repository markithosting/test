powershell_script 'Install IIS' do
  code 'Add-WindowsFeature Web-Server -IncludeAllSubfeature -IncludeManagementTools -Restart'
  not_if "(Get-WindowsFeature -Name Web-Server).Installed"
end

service 'w3svc' do
  action [:start, :enable]
end