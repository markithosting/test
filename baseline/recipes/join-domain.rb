Chef::Log.info("******Join domain******")

powershell_script 'join domain' do
  code <<-EOH
  $username = '#{node['username']}'
  $password = '#{node['password']}' | ConvertTo-SecureString -asPlainText -Force
  $domain = '#{node['domain']}'
  $credential = New-Object System.Management.Automation.PSCredential($username,$password)
  Add-Computer -domainname $domain -Credential $credential -Passthru -Verbose -Force
  EOH
  not_if '((gwmi win32_computersystem).partofdomain -eq $true)'
end