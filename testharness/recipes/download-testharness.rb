# Delete Previous Test Harness
directory 'D:\Software\TestHarness_STARTER_FILE_BASED' do
	action :delete
	recursive true
only_if {::File.exists?('D:\Software\TestHarness_STARTER_FILE_BASED')}
end


# Download Test Harness
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'edm-testharness',
                     key: 'TestHarness_STARTER_FILE_BASED.zip',
                     response_target: 'd:\software\TestHarness_STARTER_FILE_BASED.zip')
                    
  end
  action :run
  not_if {::File.exists?('D:\Software\TestHarness_STARTER_FILE_BASED.zip')}
end

# Unzip SQL Scripts
windows_zipfile 'd:\software\TestHarness_STARTER_FILE_BASED' do
  source 'D:\Software\TestHarness_STARTER_FILE_BASED.zip'
  action :unzip
  not_if {::File.exists?('D:\Software\TestHarness_STARTER_FILE_BASED\READ_ME_FIRST.txt')}
end
