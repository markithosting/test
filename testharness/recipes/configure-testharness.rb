Chef::Log.info("******Configure Test Harness XML******")

# Configure Test Harness XML
powershell_script 'Configure Test Harness XML' do
  code <<-EOH
$config = "D:\\Software\\TestHarness_STARTER_FILE_BASED\\TestHarness\\Program files\\Program files - FILE SYSTEM - local\\TestHarness_Config.xml"
$testserver = '#{node['dbdetails']['endpoint']}'
$testuser = '#{node['dbdetails']['adminUser']}'
$testpassword = '#{node['dbdetails']['adminPwd']}'
$testdatabase = 'EDM_TestHarness'
$testrootpath = '"D:\\Software\\TestHarness_STARTER_FILE_BASED\\TestHarness\\QA testing\\AutomatedTests"'
$processwaittime = '120000'
$edminstallpath = '"C:\\Program Files\\Markit Group\\Markit EDM\\"'
$setupCDpath = '"D:\\Software\\EDM"'
$setupCDtemplate = 'MarkitEDMSetupCD.zip'
$logpath = '"D:\\Software\\TestHarness_STARTER_FILE_BASED\\TestHarness\\Temp\\Log\\TestHarness_{0}.log"'
$templogdirectory = '"D:\\Software\\TestHarness_STARTER_FILE_BASED\\TestHarness\\Temp"'
$7zippath = '"C:\\Program Files\7-Zip\7z.exe" x -r -y "{0}" -o"{1}"'
$workingdirectory = "D:\\"
$summaryreportpath = '"D:\\Software\\TestHarness_STARTER_FILE_BASED\\TestHarness\\Temp\\Log\\SummaryReport_{1}.htm"'
$cadistempsetupcdpath = "{TempDirectory}\\Setup CD {0}.{1}.{2}.{3}\\"
$databaserestorepath = '"D:\\Software\\TestHarness_STARTER_FILE_BASED\\TestHarness\\TestHarnessDatabasesRestored"'
$isintegrated = 'NO'
$servicepollinterval = '30000'

Start-Sleep -Seconds 3

$xml = [xml](Get-Content $config)
$xml.Configuration.Parameters

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'EDMTestServer'}
$node.Value = $testserver

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'EDMTestUser'}
$node.Value = $testuser

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'EDMTestPassword'}
$node.Value = $testpassword

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'EDMTestDatabase'}
$node.Value = $testdatabase

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'TestRootPath'}
$node.Value = $testrootpath

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'RunProcessWaitTime'}
$node.Value = $processwaittime

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'EDMInstallationPathTemplate'}
$node.Value = $edminstallpath

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'EDMSetupKitPathTemplate'}
$node.Value = $setupCDpath

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'SetupCDTemplate'}
$node.Value = $setupCDtemplate

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'EDMInstallationPathTemplate'}
$node.Value = $edminstallpath

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'LogFileNameTemplate'}
$node.Value = $logpath

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'TempDirectory'}
$node.Value = $templogdirectory

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'UnzipCommand'}
$node.Value = $7zippath

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'WorkingDirectory'}
$node.Value = $workingdirectory

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'SummaryReportFileNameTemplate'}
$node.Value = $summaryreportpath

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'CadisTempSetupCDPath'}
$node.Value = $cadistempsetupcdpath

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'DatabaseRestorePath'}
$node.Value = $databaserestorepath

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'isintegrated'}
$node.Value = $isintegrated

$node = $xml.Configuration.Parameters | where {$_.Name -eq 'ServicePollInterval'}
$node.Value = $servicepollinterval

$xml.Save($config)

  EOH
end  