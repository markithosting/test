Chef::Log.info("******Install SSL Certificate******")

# Install Ruby gems
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# Download certificate from S3 bucket using AWS-SDK
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'
	
    s3_key = node['domain'] + '/' + node['hostname'] + '.pfx'
	
    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-ssl-certificates',
                     key: s3_key,
                     response_target: 'c:/' + node['hostname'] + '.pfx')
                    
  end
  action :run
  not_if {::File.exists?('c:/' + node['hostname'] + '.pfx')}
end

# Install the certificate into the Personal store under Computer Certificates
windows_certificate 'c:\\' + node['hostname'] + '.pfx' do
    pfx_password node['password']
end

# Looks for a certificate matching the name below in the Personal store and binds it to the website on port 443 in IIS
windows_certificate_binding node['fqdn'] do
end