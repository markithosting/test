Chef::Log.info("******Install New Relic .NET Agent******")

# Stop the IISADMIN service
windows_service 'IISADMIN' do
  action :stop
end

# Install New Relic .NET Agent
windows_package 'New Relic .NET Agent' do
  source 'http://download.newrelic.com/dot_net_agent/release/NewRelicDotNetAgent_x64.msi'
  options '/qb NR_LICENSE_KEY=eead7f4a2b126437cd9183d7b0e298fced2aeb5a INSTALLLEVEL=1'
  installer_type :msi
  action :install
  retries 3
  retry_delay 120
  not_if {::File.exists?('C:\Program Files\New Relic\.NET Agent\default_newrelic.config')}
end

powershell_script 'Instrument Config File' do
  code  <<-EOH
	# Configure variables
	$nrConfig = "C:\\ProgramData\\New Relic\\.NET Agent\\newrelic.config"
	$backup = "C:\\ProgramData\\New Relic\\.NET Agent\\newrelic.config-backup"
	$company = '#{node['aws-tag']['tags']['Company']}'
	$appName = $company+" Prod"

	# Backup current config file
	Copy-Item $nrConfig -Destination $backup

	# Read config file and set the application name
	$doc = (Get-Content $nrConfig) -replace 'My Application', $appName -as [Xml]

	# Save the file
	$doc.Save($nrConfig)
	EOH
end

# Start the IISADMIN service
windows_service 'IISADMIN' do
  action :start
end