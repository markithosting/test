Chef::Log.info("******Install New Relic******")

directory 'c:\programdata\NewRelic' do
  recursive true
  action :create
end

windows_package 'New Relic Server Monitor' do
  source 'http://download.newrelic.com/windows_server_monitor/release/NewRelicServerMonitor_x64_3.3.5.0.msi'
  options '/L*v c:\programdata\NewRelic\NewRelic_install.log /qn NR_LICENSE_KEY=eead7f4a2b126437cd9183d7b0e298fced2aeb5a'
  installer_type :msi
  action :install
  retries 3
  retry_delay 120
  not_if {::File.exists?('C:\Program Files\New Relic\Server Monitor\NewRelic.ServerMonitor.exe')}
end


