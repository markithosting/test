Chef::Log.info("******Install WinSCP and .NET Assembly******")

# install AWS SDK
chef_gem "aws-sdk" do
  compile_time false
  action :install
end

# create location for installation log file
directory 'c:\programdata\winscp' do
  recursive true
  action :create
end

directory 'd:\software' do
  action :create
end

# download winscp from S3 bucket using AWS SDK, not if already exists
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'winscp.exe',
                     response_target: 'd:\software\winscp.exe')
                    
  end
  action :run
  not_if {::File.exists?('d:\software\winscp.exe')}
end

# download winscp .net assembly from S3 bucket using AWS SDK, not of already exists
ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'winscpautomation.zip',
                     response_target: 'd:\software\winscpautomation.zip')
                    
  end
  action :run
  not_if {::File.exists?('d:\software\winscpautomation.zip')}
end

# install winscp
windows_package 'WinSCP' do
  source 'd:\software\winscp.exe'
  options '/VERYSILENT /LOG="c:\programdata\winscp\winscp.log"'
  installer_type :custom
  action :install
  retries 3
  retry_delay 120
  not_if {::File.exists?('C:\Program Files (x86)\WinSCP\WinSCP.exe')}
end

# create directory for winscp .net assembly
directory 'C:\Program Files (x86)\WinSCP\NET Assembly' do
  recursive true
  action :create
end

# unzip winscp .net assembly to above directory
windows_zipfile 'C:\Program Files (x86)\WinSCP\NET Assembly' do
  source 'D:\Software\winscpautomation.zip'
  action :unzip
  not_if {::File.exists?('C:\Program Files (x86)\WinSCP\NET Assembly\WinSCP.exe')}
end