Chef::Log.info("******Installing Trend AV******")

powershell_script 'install trend' do
  code '$env:LogPath = "$env:appdata\Trend Micro\Deep Security Agent\installer"
	New-Item -path $env:LogPath -type directory
	Start-Transcript -path "$env:LogPath\dsa_deploy.log" -append
	echo "$(Get-Date -format T) - DSA download started"
	(New-Object System.Net.WebClient).DownloadFile("https://app.deepsecurity.trendmicro.com:443/software/agent/Windows/x86_64/", "$env:temp\agent.msi")
	echo "$(Get-Date -format T) - Downloaded File Size:" (Get-Item "$env:temp\agent.msi").length
	echo "$(Get-Date -format T) - DSA install started"
	echo "$(Get-Date -format T) - Installer Exit Code:" (Start-Process -FilePath msiexec -ArgumentList "/i $env:temp\agent.msi /qn ADDLOCAL=ALL /l*v `"$env:LogPath\dsa_install.log`"" -Wait -PassThru).ExitCode 
	echo "$(Get-Date -format T) - DSA activation started"
	Start-Sleep -s 50
	& $Env:ProgramFiles"\Trend Micro\Deep Security Agent\dsa_control" -r
	& $Env:ProgramFiles"\Trend Micro\Deep Security Agent\dsa_control" -a dsm://agents.deepsecurity.trendmicro.com:443/ "tenantID:A96FD379-4ECD-8CBA-9EC4-C90131117F58" "tenantPassword:7B8FCA5D-B067-394F-4023-62FBBF650A88" "policyid:5"
	Stop-Transcript
	echo "$(Get-Date -format T) - DSA Deployment Finished"'
  retries 3
  retry_delay 120
  not_if {::File.exists?('C:\Program Files\Trend Micro\Deep Security Agent\dsa.exe')}
end