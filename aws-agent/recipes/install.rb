Chef::Log.info("******Install AWS Agent*****")


# Install AWS Agent
windows_package 'Install AWS Agent' do
  source 'https://d1wk0tztpsntt1.cloudfront.net/windows/installer/latest/AWSAgentInstall.exe'
  options '/install /quiet'
  installer_type :installshield
  action :install
  retries 1
  retry_delay 120
  not_if {::File.exists?('C:\Program Files\Amazon Web Services\AWS Agent\AWSAgent.exe')}
end