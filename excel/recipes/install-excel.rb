Chef::Log.info("******Install Excel 2013******")

chef_gem "aws-sdk" do
  compile_time false
  action :install
end

directory 'd:\software' do
  action :create
end

ruby_block "download-object" do
  block do
    require 'aws-sdk'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'

    s3_client = Aws::S3::Client.new(region:'us-east-1')

    s3_client.get_object(bucket: 'mkt-cfn-scripts',
                     key: 'Office2013.zip',
                     response_target: 'd:\software\Office2013.zip')
                    
  end
  action :run
  not_if {::File.exists?('d:\software\Office2013.zip')}
end

windows_zipfile 'd:\software' do
  source 'd:\software\Office2013.zip'
  action :unzip
  not_if {::File.exists?('d:\software\Office2013\setup.exe')}
end

windows_package 'Microsoft Excel 2013' do
  source 'D:\Software\Office2013\setup.exe'
  options '/adminfile D:\Software\Office2013\Updates\customization.msp'
  installer_type :custom
  action :install
  retries 3
  retry_delay 120
  not_if {::File.exists?('C:\Program Files (x86)\Microsoft Office\Office15\EXCEL.EXE')}
end