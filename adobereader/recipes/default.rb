Chef::Log.info("******Install Adobe Reader******")

# Create unzip destination folder
directory 'd:\software' do
  recursive true
  action :create
end

# Unzip Adobe Reader
windows_zipfile 'd:\software\adobe' do
  source 'http://ardownload.adobe.com/pub/adobe/reader/win/11.x/11.0.00/misc/AdbeRdr11000_mui_Std.zip'
  action :unzip
  not_if {::File.exists?('d:\software\adobe\Setup.exe')}
end

# Install Adobe Reader with silent install
windows_package 'adobe' do
  source 'd:\software\adobe\Setup.exe'
  options '/sAll'
  installer_type :custom
  action :install
  retries 3
  retry_delay 120
  not_if {::File.exists?('C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe')}
end

# Disable check for updates
registry_key "HKEY_LOCAL_MACHINE\\SOFTWARE\\Policies\Adobe\\Acrobat Reader\\11.0\\FeatureLockDown" do
  values [{
    :name => "bUpdater",
    :type => :dword,
    :data => 0,
    
  }]
  action :create
  recursive true
end
