Chef::Log.info("******Tag Instance******")

chef_gem "aws-sdk" do
  compile_time false
  action :install
end

ruby_block "tag-instance" do
  block do
	require 'rubygems'
	require 'aws-sdk'
	require 'net/http'
    
    Aws.config[:ssl_ca_bundle] = 'C:\ProgramData\Git\bin\curl-ca-bundle.crt'
	
	metadata_endpoint = 'http://169.254.169.254/latest/meta-data/'
	
	availability_zone = Net::HTTP.get( URI.parse( metadata_endpoint + 'placement/availability-zone' ) )
	
	region = availability_zone.slice 0..-2
	
	instance_id = Net::HTTP.get( URI.parse( metadata_endpoint + 'instance-id' ) )

	user = (node['aws-tag']['tags']['User'])
	
	company = (node['aws-tag']['tags']['Company'])
	
	service = (node['aws-tag']['tags']['Service'])
	
	costcenter = (node['aws-tag']['tags']['CostCenter'])

	ec2 = Aws::EC2::Client.new(region:region)
	
    resp = ec2.create_tags({
		resources: [instance_id],
		tags: [
			{
				key: 'User',
				value: user,
			},
			{
				key: 'Company',
				value: company,
			},
			{
				key: 'Service',
				value: service,
			},
			{
				key: 'CostCenter',
				value: costcenter,
			},
		],
	})
  end
  action :run
end